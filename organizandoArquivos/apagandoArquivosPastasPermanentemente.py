"""
pg 279 automatize tarefas maçantes

Apagando arquivos permanentemente

Podemos apagar um único arquivo ou uma única pasta vazia com funções do módulo os , ao passo que, para apagar uma pasta
e tudo o que estiver dentro dela, devemos usar o módulo shutil.

• Chamar os.unlink(path) apagará o arquivo em path.
• Chamar os.rmdir(path) apagará a pasta em path. Essa pasta deve estar vazia, ou seja, não deve conter nenhum arquivo
ou pasta.
• Chamar shutil.rmtree(path) removerá a pasta em path; além disso, todos os arquivos e as pastas nele contidos também
serão apagados.

Tome cuidado ao usar essas funções em seus programas! Geralmente, é uma boa ideia executar seu programa inicialmente
com essas chamadas comentadas, adicionando chamadas a print() para mostrar os arquivos que seriam apagados. Eis um
programa Python que deveria apagar arquivos com extensão .txt, porém com um erro de digitação que o faz apagar arquivos
.rxt:

import os
for filename in os.listdir():
    if filename.endswith('.rxt'):
        os.unlink(filename)

Se houvesse algum arquivo importante terminado com .rxt, eles teriam sido apagados acidentalmente de forma permanente.
Em vez disso, você deve inicialmente executar o programa da seguinte maneira:

import os
for filename in os.listdir():
    if filename.endswith('.rxt'):
        #os.unlink(filename)
        print(filename)
"""

#Saiba que esse é uma forma de apagar arquivos de modo irreversível. Veja uma forma mais segura de apagar usando o
# send2trash aqui nessa pasta organizando arquivos.