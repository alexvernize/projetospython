"""
Suponha que você queira renomear todos os arquivos de alguma pasta, além de todos os arquivos de todas as subpastas
dessa pasta. Isso quer dizer quevocê deverá percorrer a árvore de diretório, alcançando cada arquivo nesse processo.
Com a função os.walk() isso é possível.

A função os.walk() recebe um único valor de string: o path de uma pasta. os.walk() pode ser usada em uma instrução
de loop for para percorrer uma árvore de diretório, de modo muito semelhante à forma como a função range() é utilizada
para percorrer um intervalo de números. De modo diferente de range() , a função os.walk() retornará três valores a
cada iteração pelo loop:

1. Uma string com o nome da pasta atual.
2. Uma lista de strings com as pastas da pasta atual.
3. Uma lista de strings com os arquivos da pasta atual.
"""
import os

#Com a função os.walk() retorna três valores, precisamos de três variáveis.
for nomePasta, subPastas, nomeArquivos in os.walk('/home/alex/Pictures'):
    print('A pasta atual é ' + nomePasta)

    for subPasta in subPastas:
        print('A subpasta de ' + nomePasta + ':' + subPasta)

    for nomeArquivo in nomeArquivos:
        print('Arquivo dentro ' + nomePasta + ':' + nomeArquivo)

    print('')