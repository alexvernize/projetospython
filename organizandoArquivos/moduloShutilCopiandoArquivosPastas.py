"""
pag 277 automatize tarefas maçantes

O módulo shutil (shell utilities, ou utilitários de shell) contém funções que permitem copiar, mover, renomear
e apagar arquivos em seus programas Python. Para usar as funções de shutil, inicialmente você deverá usar import
shutil.
"""
import shutil, os

#Chamar shutil.copy(origem, destino) copiará o arquivo no path origem para a pasta no path destino.

#Primeiro dizemos qual o caminho
os.chdir('/home/alex/')

#O shutil.copy copia um arquivo, é usado dois argumentos mo primeiro especificamos qual arquivo queremos copiar e
#o segundo argumento, para onde queremos copiar. Podemos tanto especificar um novo nome, quanto não colocar um nome
#dessa forma o nome original do arquivo será copiado. Essa função retorna uma string com o path do arquivo copiado.
shutil.copy("/home/alex/teste.txt", "/home/alex/teste/")

#Enquanto shutil.copy() copia um único arquivo, shutil.copytree() copiará uma pasta completa, com todas as pastas e
#os arquivos contidos. Chamar shutil.copytree(origem, destino) copiará a pasta no path origem, juntamente com todos
#os seus arquivos e suas subpastas, para a pasta no path destino. Os parâmetros origem e destino são strings. A
#função retorna uma string com o path da pasta copiada.
shutil.copytree('/home/alex/teste/', '/home/alex/Documents/teste_backup/')



