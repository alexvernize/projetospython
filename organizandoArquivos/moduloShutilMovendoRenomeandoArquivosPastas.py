#shutil.move(origem, destino) moverá o arquivo ou a pasta no path origem para o path destino e retornará uma string
#com o path absoluto da nova localidade. Da mesma forma que os outros, você pode criar um nome novo, renomeando o
#arquivo, ou apenas escolher a pasta dessa forma o nome do arquivo permanecerá.
import shutil, os

#Primeiro dizemos qual o caminho
os.chdir('/home/alex/')

shutil.move("/home/alex/teste.txt", "/home/alex/teste/teste3.txt")
"""
Não esqueça que esse comando move um arquivo, diferentemente do copy que copia e cria um arquivo novo. Então se for 
mover um arquivo e criar um novo nome, nunca esqueça da extensão dele, pois se colocar apenas um nome sem extensão
ele será criado dessa forma.
"""