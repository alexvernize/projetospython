#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Como a função interna shutil.rmtree() do Python apaga arquivos e pastas de modo irreversível, pode ser perigoso
usá-la. Uma maneira muito melhor de apagar arquivos e pastas consiste em usar o módulo de terceiros send2trash.
Esse módulo pode ser instalado por meio da execução de pip install send2trash em uma janela do Terminal.
'''

import send2trash

arquivo = open('arquivo.txt', 'a')#Cria o arquivo

arquivo.write('Escrevendo qualquer coisa para testar.')#Escrevendo no arquivo

arquivo.close()#Fechando o arquivo

send2trash.send2trash('arquivo.txt')#Apagando e enviando para a lixeira

'''
Utilizando o send2trash.send2trash() você envia o arquivo que quer deletar para a lixeira, dessa forma pode recuperar
se achar necessário. 
'''