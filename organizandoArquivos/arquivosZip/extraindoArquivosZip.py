"""
O método extractall() de objetos ZipFile extrai todos os arquivos e as pastas de um arquivo ZIP no diretório de
trabalho atual.
"""
#Primeiro importamos as bibliotecas que iremos usar.
import zipfile, os

#Com o os.chdir() adicionamos o path (local do arquivo), onde o arquivo zip está localizado
os.chdir('/home/alex/teste')

#Com zipfile.ZipFile() dizemos qual o arquivo queremos que seja extraído
exemploZip = zipfile.ZipFile('texto.zip')

#Usamos o extract() para extrair os arquivos
exemploZip.extractall()
"""
Opcionalmente, um nome de pasta pode ser passado para extractall() para que esse método extraia os arquivos em uma 
pasta que não seja o diretório de trabalho atual. Se a pasta passada para o método extractall() não existir, ela
será criada. Exemplo:

exemplo2Zip = zipfile.ZipFile('texto.zip')
exemplo2Zip.extractall('/home/alex/novaPasta')
exemplo2Zip.close()

Também existe o método extract() que extrai um único arquivo do arquivo ZIP.

exampleZip.extract('spam.txt')
exampleZip.extract('spam.txt', 'C:\\some\\new\\folders')
exampleZip.close()

A string passada para extract() deve coincidir com uma das strings da lista retornada por namelist(). Opcionalmente, 
um segundo argumento pode ser passado a extract() para extrair o arquivo em uma pasta que não seja o diretório de 
trabalho atual. Se esse segundo argumento for uma pasta que ainda não exista, o Python a criará. O valor retornado 
por extract() é o path absoluto em que o arquivo foi extraído.
"""

#Por fim fechamos o arquivo
exemploZip.close()