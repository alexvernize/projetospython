"""
Para criar seus próprios arquivos ZIP compactados, abra o objeto ZipFile em modo de escrita passando 'w' como segundo
argumento. (Isso é semelhante a abrir um arquivo-texto em modo de escrita passando 'w' à função open()). Ao passar um
path para o método write() de um objeto ZipFile, o Python compactará o arquivo nesse path e o adicionará ao arquivo
ZIP. O primeiro argumento do método write() é uma string que contém o nome do arquivo a ser adicionado. O segundo
argumento é o parâmetro referente ao tipo de compactação, que diz ao computador qual algoritmo deverá ser usado para
compactar os arquivos; você poderá simplesmente definir esse valor sempre com zipfile.ZIP_DEFLATED . (Isso especifica
o algoritmo de compressãodeflate, que funciona bem para todos os tipos de dados.)
"""
#Importar bibliotecas
import zipfile, os

#Com o os.chdir() adicionamos o path (local do arquivo), onde o arquivo zip está localizado
os.chdir('/home/alex/teste')

#Dois argumentos, o nome do arquivo 'arquivo.zip' e o modo escrita 'w'
novoZip = zipfile.ZipFile('arquivo.zip', 'w')

"""
Não esquecer que o modo escrita apagará qualquer conteúdo existente em arquivo Zip. Se quiser simplesmente adicionar 
arquivos em um arquivo ZIP existente, passe 'a' como o segundo argumento de zipfile.ZipFile() para que o arquivo ZIP 
seja aberto em modo de adição.
"""

#novoZip2 = zipfile.ZipFile('arquivo.zip', 'a')

#Dois argumentos em write arquivo que queremos zipar e o algoritmo que estamos usando
novoZip.write('texto1.txt', compress_type=zipfile.ZIP_DEFLATED)
#novoZip2.write('texto2.txt', compress_type=zipfile.ZIP_DEFLATED)

#Fechando o arquivo
novoZip.close()
#novoZip2.close()


