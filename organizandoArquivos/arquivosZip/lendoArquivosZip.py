'''
pg 285 automatize tarefas maçantes

Arquivos com extensão .zip são arquivos compactados. Para ler o conteúdo de um arquivo ZIP, inicialmente você deve
criar um objeto ZipFile (observe as letras Z e F maiúsculas). Para criar um objeto ZipFile, chame a função
zipfile.ZipFile() passando-lhe uma string com o nome do arquivo .zip.
'''
import zipfile, os

#O caminho até o arquivo .zip
os.chdir('/home/alex/teste')

#Aqui você especifica o nome do arquivo zip que quer ler, note que zipfile é o nome do módulo e ZipFile a função
exemploZip = zipfile.ZipFile('arquivo.zip')

#Aqui listamos os arquivos contidos dentro do .zip
print(exemploZip.namelist())
#Um objeto ZipFile tem um método namelist() que retorna uma lista de strings com todos os arquivos e pastas contidos
#no arquivo ZIP.

#Agora podemos descobrir algumas informações sobre os arquivos dentro do zip.

#Podemos passar o texto1.txt para o método getinfo() dessa forma conseguimos informações sobre o tamanho do arquivo
# sem compressão e comprimido em bytes.
infoTexto1 = exemploZip.getinfo('texto1.txt')

#Usando o file_size na variável infoTexto1, obtemos o tamanho do arquivo sem compressão.
print(infoTexto1.file_size)

#Usando o compress_size na variável infoTexto1, obtemos o tamanho do arquivo sem compressão.
print(infoTexto1.compress_size)

#print('O arquivo comprimido é %sx menor!')%(round(infoTexto1.file_size/infoTexto1.compress_size, 2))