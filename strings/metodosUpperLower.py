'''
Os métodos de string upper() e lower() retornam uma nova string em que todas 
as letras da string original foram convertidas para letras maiúsculas ou
minúsculas, respectivamente.
'''

string = "O céu resplandesce e ao meu redor"
print(string)

#Repare na sintaxe do uso do método
string = string.upper()
print(string)

string2 = "DIZ AÍ COMO É QUE É, FOGO NA BOMBA"
print(string2)
string2 = string2.lower()
print(string2)

'''
Observe que esses métodos não alteram a string em si, mas retornam novos
valores de string. Se quiser alterar a string original, será necessário chamar
upper() ou lower() na string e, em seguida, atribuir a nova string à variável em
que a original estava armazenada. É por isso que devemos usar spam =
spam.upper() para alterar a string em spam no lugar de utilizar simplesmente
spam.upper() . (É como se uma variável eggs contivesse o valor 10 . Escrever eggs
+ 3 não altera o valor de eggs , porém eggs = eggs + 3 o modifica.)
'''

'''
Como os métodos de string upper() e lower() retornam strings, também
podemos chamar os métodos de string nesses valores de string retornados.
Veja abaixo como isso funciona.
'''

print('hello'.upper())
print('HELLO'.lower())
