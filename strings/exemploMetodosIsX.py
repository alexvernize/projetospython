'''
Os métodos de string isX são úteis para validar dados de entrada do usuário.
Por exemplo, o programa a seguir pergunta repetidamente aos usuários a
idade e pede uma senha até que dados de entrada válidos sejam fornecidos.
'''

#Só irá sair do laço se for digitada uma idade apenas com caracteres numéricos.
while True:
    print('Digite sua idade: ')
    idade = input()
    #Repare que é usado o método isdecimal() para aceitar apenas números.
    if idade.isdecimal():
        break
    print('Por favor digite valores numéricos para a idade.')

#Só sairá do loop se a senha digitada for apenas com carateres numéricos e letras 
while True:
    print('Digite uma nova senha (apenas letras e números):')
    password = input()
    #Aqui é usado o método isanum() que permite apenas letras e números.
    if password.isalnum():
        break
    print('Senhas podem conter apenas letras e números.')
