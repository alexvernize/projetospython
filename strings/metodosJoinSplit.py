'''
O método join() é útil quando temos uma lista de strings que devem ser unidas
em um único valor de string. O método join() é chamado em uma string,
recebe uma lista de strings e retorna uma string. A string retornada
corresponde à concatenação de todas as strings da lista passada para o
método.
'''

#Observe que a string onde join() é chamada é inserida entre cada string do 
#argumento da lista.

#Aqui a string passada é a vírgula ','
print(','.join(['cats', 'rats', 'bats']))

#Aqui a string passada é um espaço
print(' '.join(['Meu', 'nome', 'é', 'Álex.']))

#Só como exemplo vamos colocar vazio para o print de cima
print(''.join(['Meu', 'nome', 'é', 'Álex.']))


'''
O método split() faz o inverso: é chamado em um valor de string e retorna uma 
lista de strings. Veja exemplos abaixo.
'''

print('Meu nome é Álex.'.split())

#É possível passar um string delimitadora ao método split, para especificar onde 
#queremos separar uma string da outra.

print('MeuTESTEnomeTESTEéTESTEÁlex.'.split('TESTE'))

#Um uso comum de split() está em dividir uma string de múltiplas linhas nos
#caracteres de quebra de linha.

texto = '''Este é um texto de teste
com quebras de páginas 
para o método split()
ser usado e delimitar a 
string nas quebras de 
página.'''
print(texto.split('\n'))
