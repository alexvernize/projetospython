'''
Os métodos upper() e lower() serão úteis caso seja necessário fazer uma
comparação sem levar em conta a diferença entre letras maiúsculas e
minúsculas. As strings 'great' e 'GREat' não são iguais. Veja o exemplo
abaixo.
'''

print("Como você está?")
sentimento = input()
if sentimento.lower() == 'bem':
    print('Eu estou bem também.')
else:
    print('Eu espero que você se foda.')
