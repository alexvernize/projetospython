'''
Os operadores in e not in podem ser usados com strings, assim como em 
valores de lista. Uma expressão com duas strings unidas por meio de in 
ou de not in será avaliada como um booleano True ou False .
'''

string = "Hello World!"

print('Hello' in string)
print('a' not in string)

#Essas expressões testam letras maiúsculas e minúsculas
print('HELLO' in string)
