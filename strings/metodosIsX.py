'''
Juntamente com islower() e isupper() , há diversos métodos de string cujos
nomes começam com a palavra is. Esses métodos retornam um valor
booleano que descreve a natureza da string. Veja alguns exemplos abaixo.
'''

#isalpha() retornará True se a string for constituída somente de letras e não
#estiver vazia.

print('hello'.isalpha())

#isalnum() retornará True se a string for constituída somente de letras e
#números e não estiver vazia.

print('hello123'.isalnum())

#isdecimal() retornará True se a string for constituída somente de caracteres
#numéricos e não estiver vazia.

print('12345'.isdecimal())

#isspace() retornará True se a string for constituída somente de espaços,
#tabulações e quebras de linha e não estiver vazia.

print('     '.isspace())

#istitle() retornará True se a string for constituída somente de palavras que
#comecem com uma letra maiúscula seguida somente de letras minúsculas.

print("Isso É Um Título!".istitle())
