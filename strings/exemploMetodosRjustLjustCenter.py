'''
Programa para exibir uma lista de itens e seus respectivos valores tabulados.
'''


def printMercado (itensMercado, larguraEsquerda, larguraDireita):
    print('Itens do Mercado'.center(larguraEsquerda + larguraDireita, '-'))
   
    #Como visto em dicionários o método items() devolve os valores  e chaves 
    #de um dicionário
    for k, v in itensMercado.items():
        print(k.ljust(larguraEsquerda, '.') + str(v).rjust(larguraDireita))

mercado = {'Maçãs': 4, 'Laranjas': 8}
printMercado(mercado, 12, 5)

