'''
Os métodos isupper() e islower() retornarão um valor booleano True 
caso a string tenha todas as letras maiúsculas ou minúsculas, 
respectivamente. Veja abaixo o funcionamento.
'''

string = "Hello World"
#Repare que pela string ter a primeira letra maiúscula o retorno booleano
#é False
print(string.islower())
print(string.isupper())

string2 = "hello world"

print(string2.islower())

string3 = "HELLO WORLD"
print(string3.isupper())

'''
Como os métodos de string upper() e lower() retornam strings, também 
podemos chamar os métodos de string nesses valores de string retornados.
Veja abaixo como isso funciona.
'''

print('hello'.islower())
print('HELLO'.isupper())


