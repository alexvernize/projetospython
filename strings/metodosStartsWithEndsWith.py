'''
Os métodos startswith() e endswith() retornarão True se o valor de string com o
qual forem chamados começar ou terminar (respectivamente) com a string
passada para o método; do contrário, retornarão False. Veja abaixo.
'''

print('Hello World'.startswith('Hello'))
print('Hello World'.endswith('World'))
print('acbd'.startswith('abcde'))
