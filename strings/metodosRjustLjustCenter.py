'''
Os métodos de string rjust() e ljust() retornam uma versão preenchida da string
em que são chamados, com espaços inseridos para justificar o texto. O
primeiro argumento de ambos os métodos é um inteiro referente ao tamanho
da string justificada. veja abaixo exemplos desses métodos
'''

'''
'Álex.rjust(10)' diz que queremos justificar 'Hello' à direita em uma string de
tamanho total igual a 10. Álex tem 4 caracteres, portanto 6 espaços
serão acrescentados à sua esquerda, resultando em uma string de dez
caracteres, com 'Álex' justificado à direita.
'''
print('Álex'.rjust(10))


print('Álex'.rjust(20))
print('Álex'.ljust(10))
print('Álex'.ljust(20))

#Um segundo argumento opcional de rjust() e ljust() especifica um caractere
#de preenchimento que não seja um caractere de espaço.
print('Álex'.rjust(10, '*'))
print('Álex'.ljust(10, '#'))

#O método de string center() funciona como ljust() e rjust() , porém centraliza o
#texto em vez de justificá-lo à esquerda ou à direita.

print('Álex'.center(20))
print('Álex'.center(20, '*'))


