'''
O método strip() remove os espaços em branco de uma string do início ou do fim da mesma.
Os métodos lstrip() e rstrip() removerão caracteres de espaços em branco das extremidades
esquerda e direita, respectivamente. Veja exemplos abaixo.
'''

string = '   Teste de string com espaços no início e no fim      '
print(string)
print(string.strip())
print(string.lstrip())
print(string.rstrip())

'''
Opcionalmente, um argumento do tipo string especificará quais caracteres
deverão ser removidos das extremidades.
'''

string2 = 'Teste som um dois tres testando som'
#Não importa a sequência dos caracteres que forem colocados como argumentos
print(string2.strip('smo'))