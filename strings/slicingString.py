'''
Assim como as listas as strings também usam índices e slices. Podemos 
pensar numa string como uma lista e cada caractere da string como sendo 
um item com um índice correspondente. Veja abaixo um exemplo.
'''

string = 'Hello World!'

print(string)

#O primeiro item da string, assim como na lista é o '0'
print(string[0])

#Como dito acima é possível fazer um slice do mesmo modo que em listas
print(string[0:5])
