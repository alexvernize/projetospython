#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 20:37:27 2019

@author: alexvernize@gmail.com
"""

'''
O módulo pyperclip tem funções copy() e paste() capazes de enviar e receber
texto do clipboard (área de transferência) de seu computador. Enviar a saída
de seu programa para o clipboard facilitará colá-lo em um email, um
processador de texto ou em outro software.
'''
import pyperclip

#Utilizando esses métodos estamos colocando no clipboard o texto da string.
pyperclip.copy('Álex Vernize.')

#Não é necessário printar para deixar no clipboard, apenas o método copy e 
#paste são suficientes. 'pyperclip.paste()'
print(pyperclip.paste())

