'''
Existem muitas coisas que podem ser feitas com strings em python.
Dentre elas podemos concatenar strings utilizando o operador +.
Veja um exemplo abaixo.
'''

stringA = "Alex & "

stringB = "Dayane"

print(stringA + stringB)
