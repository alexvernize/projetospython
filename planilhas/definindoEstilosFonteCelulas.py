##! python3
#Pg 389 automatize tarefas macantes

"""
Para personalizar os estilos das fontes nas células, é preciso importar as funções Font() and Style() do
módulo openpyxl.styles. Isso permite digitar Font() no lugar de openpyxl.styles.Font().
"""
import openpyxl
from openpyxl.styles import Font, NamedStyle
from openpyxl.styles import colors

#Criando um arquivo
wb = openpyxl.Workbook()

#Selecionando a planilha
sheet = wb['Sheet']

highlight = NamedStyle(name="highlight")

highlight.font = Font(bold=True, italic=True, size=14, color=colors.BLUE, name='Cambria')

sheet['A1'].style = highlight

sheet['A1'] = 'Hello world!'

wb.save('styled.xlsx')

#Para mais detalhes sobre o modulo consulte a documantacao em http://openpyxl.readthedocs.org/.


