##! python3
#Pg 385 automatize tarefas macantes

"""
Seu programa deve fazer o seguinte:

• Percorrer todas as linhas em um loop.
• Se a linha contiver alho (garlic), aipo (celery) ou limões (lemons), o preço deverá ser alterado.

Isso significa que seu código deverá fazer o seguinte:

• Abrir o arquivo com a planilha.
• Para cada linha, verificar se o valor na coluna A é Celery, Garlic ou Lemon.
• Em caso afirmativo, o preço na coluna B deverá ser atualizado.
• Salvar a planilha em um novo arquivo (para que você não perca a planilha antiga, somente por garantia).
"""

import openpyxl

wb = openpyxl.load_workbook('produceSales.xlsx')

sheet = wb['Sheet']

# Os tipos de produto e seus preços atualizados
PRICE_UPDATES = {'Garlic': 3.07,
                 'Celery': 1.19,
                 'Lemon': 1.27}

#Percorremos as linhas em um loop a partir da linha 2, pois a linha 1 contém apenas o cabeçalho.
for rowNum in range(2, sheet.max_row):

    #A célula na coluna 1 (ou seja, na coluna A) será armazenada na variável produceName.
    produceName = sheet.cell(row=rowNum, column=1).value

    #Se produceName estiver presente como uma chave no dicionário PRICE_UPDATES w, saberemos que essa é uma linha
    #que deverá ter seu preço corrigido.
    if produceName in PRICE_UPDATES:

        #O preço correto estará em PRICE_UPDATES[produceName].
        sheet.cell(row=rowNum, column=2).value = PRICE_UPDATES[produceName]

wb.save('updatedProduceSales.xlsx')

"""
Ideias para programas semelhantes
Como muitos funcionários de escritório usam planilhas Excel o tempo todo,
um programa que possa editar e gravar arquivos Excel automaticamente
poderá ser muito útil. Um programa desse tipo pode fazer o seguinte:

• Ler dados de uma planilha e escrevê-los em partes de outras planilhas.
• Ler dados de sites, arquivos-texto ou do clipboard e gravá-los em uma planilha.
• “Limpar” dados de planilhas automaticamente. Por exemplo, poderíamos

usar expressões regulares para ler vários formatos de números de telefone e
editá-los em um formato único e padronizado.
"""