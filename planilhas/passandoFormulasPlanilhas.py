##! python3
#Pg 391 automatize tarefas macantes
import openpyxl

wb = openpyxl.Workbook()

sheet = wb.active

sheet['A1'] = 200
sheet['A2'] = 300
sheet['A3'] = '=SUM(A1:A2)'

wb.save('formula.xlsx')

wbFormula = openpyxl.load_workbook('formula.xlsx')

sheet1 = wbFormula.active

#Dessa forma é possível ver a fórmula
print(sheet1['A3'].value)

#Dessa forma vemos o resultado da fórmula
wbDataOnly = openpyxl.load_workbook('formula.xlsx', data_only=True)
sheet2 = wbDataOnly.active
print(sheet2['A3'].value)