#! python3
#Projeto: Ler dados de uma planilha
#Pg 376 automatize tarefas macantes
# readCensusExcel.py – Cria uma tabela com a população e o número de setores censitários de cada condado.

"""
Eis o que o seu programa deve fazer:
• Ler os dados da planilha Excel.
• Contabilizar o número de setores censitários em cada condado.
• Contabilizar a população total de cada condado.
• Exibir os resultados.

Isso significa que seu código deverá fazer o seguinte:
• Abrir e ler as células de um documento Excel com o módulo openpyxl.
• Fazer os cálculos com os dados dos setores censitários e da população e armazenar os resultados em uma
estrutura de dados.
• Gravar a estrutura de dados em um arquivo-texto com extensão .py usando o módulo pprint.

"""
import openpyxl, pprint

print('Abrindo workbook...')

wb = openpyxl.load_workbook("censuspopdata.xlsx")

sheet = wb["Population by Census Tract"]

countyData = {}

""" TODO: Preenche countyData com a população e os setores de cada condado. """
print('Reading rows...')

for row in range(2, sheet.get_highest_row() + 1):
    state = sheet['B' + str(row)].value
    county = sheet['C' + str(row)].value
    pop = sheet['D' + str(row)].value

    # Garante que a chave para esse estado existe.
    countyData.setdefault(state, {})

    # Garante que a chave para esse condado nesse estado existe.
    countyData[state].setdefault(county, {'tracts': 0, 'pop': 0})

    # Cada linha representa um setor censitário, portanto incrementa o valor de um.
    countyData[state][county]['tracts'] += 1

    # Soma a população desse setor censitário à população do condado.
    countyData[state][county]['pop'] += int(pop)

print('Escrevendo Resultados...')
resultFile = open('census2010.py', 'w')
resultFile.write('allData = ' + pprint.pformat(countyData))
resultFile.close()
print('Feito.')

"""
A função pprint.pformat() gera uma string formatada como código Python
válido. Ao gravá-la em um arquivo-texto chamado census2010.py, você terá
gerado um programa Python com o seu programa Python. A partir de agora, será possível
importar census2010.py, como qualquer outro módulo Python.
"""