##! python3
#Pg 382 automatize tarefas macantes

import openpyxl

#Chame a função openpyxl.Workbook() para criar um novo objeto Workbook vazio. Diferente do load_workbook que carrega
#um arquivo xlsx
wb = openpyxl.Workbook()

#sheetname para exibir o nome das planilhas
print(wb.sheetnames)

#active para trabalhar com a planilha que está selecionada
sheet = wb.active

#title para mostrar o nome da aba que está ativa
print(sheet.title)

#Alterando o nome da planilha
sheet.title = 'Teste de mudança de nome de aba'

print(wb.sheetnames)

'''
O workbook será iniciado com uma única planilha chamada Sheet. O nome
da planilha poderá ser alterado se uma nova string for armazenada em seu
atributo title.
'''

#Salvando o arquivo excel
wb.save('teste.xlsx')


