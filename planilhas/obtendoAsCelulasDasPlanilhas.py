#Pagina 370 automatize tarefas macantes

import openpyxl

wb = openpyxl.load_workbook('example.xlsx')

print(wb.sheetnames)

sheet = wb['Sheet1']

print(sheet['A1'])

print(sheet['A1'].value)

c = sheet['B1']

print(c.value)

print('O valor na linha ' + str(c.row) + ', coluna ' + str(c.column) + ' é ' + str(c.value))

print('O valor da celula ' + str(c.coordinate) + ' é ' + str(c.value))

print('O valor da linha 1 coluna 3 é: ', sheet['C1'].value)

"""
O objeto Cell tem um atributo value que contém o valor armazenado nessa célula. Os objetos Cell também têm atributos 
row, column e coordinate que fornecem informações sobre a localização da célula.
"""

"""
Especificar uma coluna pela letra pode ser complicado para programar, especialmente porque, após a coluna Z, as colunas 
começam a usar duas letras: AA, AB, AC e assim por diante. Como alternativa, podemos também obter uma célula usando o 
método cell() da planilha e passando inteiros para seus argumentos nomeados row e column. O inteiro correspondente à 
primeira linha ou coluna é 1 , e não 0.
"""
print(sheet.cell(row=1, column=2))
print(sheet.cell(row=1, column=2).value)

"""
percorrer a coluna B e exibir o valor de todas as células que tenham um número de linha ímpar. Ao passar 2 para o 
parâmetro de “incremento” da função range() , podemos obter as células a cada duas linhas (nesse caso, todas as linhas 
com números ímpares). A variável i do loop for é passada para o argumento nomeado row do método cell() , enquanto 2
é sempre passado para o argumento nomeado column. Observe que passamos o inteiro 2 , e não a string 'B'.
"""
for i in range(1, 8, 2):
    print(i, sheet.cell(row=i, column=2).value)


#Podemos determinar o tamanho da planilha usando os métodos get_highest_row() e get_highest_column() do objeto Worksheet.
print(sheet.max_row)
print(sheet.max_column)