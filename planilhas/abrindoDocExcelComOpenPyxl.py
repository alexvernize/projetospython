#Pagina 369 automatize tarefas macantes

import openpyxl

#openpyxl.load_workbook() recebe o nome do arquivo e retorna um valor com o tipo de dado workbook.
wb = openpyxl.load_workbook('example.xlsx')
print(type(wb))

"""
Lembre-se de que example.xlsx deve estar no diretório de trabalho atual para que você possa trabalhar com ele. Você 
pode descobrir qual é o diretório de trabalho atual importando o módulo os e usando os.getcwd(); além disso, o
diretório de trabalho atual poderá ser alterado com os.chdir().
"""