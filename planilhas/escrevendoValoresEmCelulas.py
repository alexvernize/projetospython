##! python3
#Pg 384 automatize tarefas macantes
import openpyxl

wb = openpyxl.Workbook()

sheet = wb['Sheet']

sheet['A1'] = 'Hello World!'

print(sheet['A1'].value)

"""
Se você tiver a coordenada da célula como uma string, poderá usá-la como
se fosse uma chave de dicionário no objeto Worksheet para especificar em qual
célula a escrita será feita.
"""