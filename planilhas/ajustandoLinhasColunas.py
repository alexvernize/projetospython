##! python3
#Pg 393 automatize tarefas macantes
import openpyxl

wb = openpyxl.Workbook()

sheet = wb.active

sheet['A1'] = "Aumentando linha"

sheet['B2'] = "Alargando Coluna"

#O comando abaixo aumenta a linha
sheet.row_dimensions[1].height = 70

#O comando abaixo alarga a coluna
sheet.column_dimensions['B'].width = 20

wb.save('dimensoes.xlsx')