#Pagina 370 automatize tarefas macantes

import openpyxl

wb = openpyxl.load_workbook('example.xlsx')

#Uma lista com os nomes de todas as planilhas do workbook pode ser obtida por meio da chamada ao método sheetnames
print(wb.sheetnames)

#Antigamente era utilizado get_sheet_names, mas ja esta em desuso
#print(wb.get_sheet_names())

#Cada planilha é representada por um objeto Worksheet, que pode ser obtido se passarmos a string com o nome da planilha
sheet = wb['Sheet3']
print(sheet)

print(type(sheet))

#De posse do objeto Worksheet, você poderá obter o seu nome a partir do atributo title.
print(sheet.title)

#O metodo 'active' nos mostra a planilha ativa do workbook, a planilha ativa é aquela que estará em evidência quando o
#workbook for aberto no Excel.
outraSheet = wb.active

print(outraSheet)