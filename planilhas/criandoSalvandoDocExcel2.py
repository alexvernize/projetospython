##! python3
#Pg 382 automatize tarefas macantes

import openpyxl

#Diferente do primeiro arquivo, aqui vamos carregar um arquivo excel e alterar o nome da planilha, depois salvamos
#com outro nome
wb = openpyxl.load_workbook('example.xlsx')

#sheetname para exibir o nome das planilhas
print(wb.sheetnames)

#active para trabalhar com a planilha que está selecionada
sheet = wb.active

#title para mostrar o nome da aba que está ativa
print(sheet.title)

#Alterando o nome da planilha
sheet.title = 'Teste de mudança de nome de aba'

print(wb.sheetnames)

'''
O workbook será iniciado com uma única planilha chamada Sheet. O nome
da planilha poderá ser alterado se uma nova string for armazenada em seu
atributo title.
'''

#Salvando o arquivo excel
wb.save('example_copy.xlsx')