#pagina 374 automatize tarefas macantes
import openpyxl

wb = openpyxl.load_workbook('example.xlsx')

sheet = wb['Sheet1']

print(str(tuple(sheet['A1':'C3'])) + '\n')

#Percorre todas as linhas do slice
for rowOfCellObjects in sheet['A1':'C3']:
    #Percorre todas as celulas dessa linha
    for cellObj in rowOfCellObjects:
        print(cellObj.coordinate, cellObj.value)
    print('--- END OF ROW ---')

"""
Para acessar os valores das células de uma linha ou de uma coluna em
particular, também podemos usar os atributos rows e columns de um objeto
Worksheet.
"""

sheet2 = wb.active

#O openpyxl mudou em relacao ao livro, a forma correta de uso eh a que esta abaixo, repare que eu seleciono
#a coluna "B" e apenas coloco entre colchetes. Depois a variavel .value
print(sheet2["A"])

for i in sheet2["B"]:
    print(i.value)