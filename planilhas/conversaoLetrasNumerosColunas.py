#pagina 373 automatize tarefas macantes
import openpyxl
try:
    from openpyxl.cell import get_column_letter, column_index_from_string
except ImportError:
    from openpyxl.utils import get_column_letter, column_index_from_string

print(get_column_letter(1))
print(get_column_letter(2))
print(get_column_letter(27))
print(get_column_letter(900))

wb = openpyxl.load_workbook('example.xlsx')

sheet = wb['Sheet1']

print(get_column_letter(sheet.max_column))

print(column_index_from_string('A'))

print(column_index_from_string('AA'))

"""
pós ter importado essas duas funções do módulo openpyxl.cell, podemos chamar a função get_column_letter() e passar-lhe 
um inteiro, por exemplo, 27, para descobrir qual é a letra da 27a coluna. A função column_index_string() faz o
inverso: ela recebe a letra referente a uma coluna e informa o número dessa coluna. Não é necessário ter um workbook 
carregado para usar essas funções. Se quiser, você poderá carregar um workbook, obter um objeto Worksheet e chamar um 
método desse objeto, por exemplo, get_highest_column(), para obterum inteiro. Então você poderá passar esse inteiro 
para get_column_letter().
"""