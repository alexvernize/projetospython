##! python3
#Pg 383 automatize tarefas macantes
import openpyxl

#Criando um arquivo novo com uma planilha
wb = openpyxl.Workbook()

#Lista as planilhas
print(wb.sheetnames)

#Cria uma planilha nova, porém é criada após a primeira planilha, no caso se torna a segunda
wb.create_sheet()

#Lista as planilhas
print(wb.sheetnames)

#Cria uma planilha na posição '0', no caso será a primeira planilha, como o nome de Primeira planilha
wb.create_sheet(index=0, title='Primeira planilha')

#Lista as planilhas
print(wb.sheetnames)

#Cria uma planilha na posição '2', com o nome de Planilha do meio
wb.create_sheet(index=2, title='Planilha do meio')

#Lista as planilhas
print(wb.sheetnames)

"""
O método create_sheet() retorna um novo objeto Worksheet chamado SheetX,
que, por padrão, é definido como a última planilha do workbook.
Opcionalmente, o índice e o nome da nova planilha podem ser especificados
por meio dos argumentos nomeados index e title.
"""

#Remove a planilha 'Planilha do meio'
wb.remove(wb['Planilha do meio'])

#Lista as planilhas
print(wb.sheetnames)

#Remove a planilha 'Sheet1'
wb.remove(wb['Sheet1'])

#Lista as planilhas
print(wb.sheetnames)

"""
O método remove_sheet() aceita um objeto Worksheet como argumento, e não
uma string com o nome da planilha. Se apenas o nome de uma planilha que
você quer remover for conhecido, chame get_sheet_by_name() e passe seu valor
de retorno para remove_sheet().
"""

"""
Lembre-se de chamar o método save() para salvar as alterações após ter
adicionado ou removido planilhas do workbook.
"""
