#Diferente do laço while (que significa enquanto, no caso enquanto a 
#condição for verdadeira continue fazendo) o laço for é destinado 
#em geral a executar um bloco de código determinadas vezes.
print('Meu nome é: ')
for i in range(5):
    print ('Alex cinco vezes ('+ str(i) + ')')
