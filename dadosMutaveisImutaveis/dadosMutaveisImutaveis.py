'''
Listas e strings apesar de serem parecidos possuem algumas grandes diferenças, 
dentre elas a ideia de dado mutável e imutável. Um valor de lista é um tipo de 
dado mutável, ou seja, ele pode ter valores adicionados, removidos ou alterados.
Já strings possuem valores imutáveis, não pode ser alterada.

nome = 'Alex tem um gato'
nome[5] = 'tinha'

Esse código acima gera um erro, porque não é possível mudar uma string dessa forma.
A maneira apropriada de efetuar uma “mutação” em uma string é usar slicing e 
concatenação para criar uma nova string, copiando partes da string antiga. Veja 
abaixo exemplos.
'''

nome = 'Alex tem um gato'
novoNome = nome[0:5] + 'tinha' + nome [8:16]
print(nome)
print(novoNome)

