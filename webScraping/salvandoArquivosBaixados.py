"""
Pagina 333 automatize tarefas macantes
Apos aprender a baixar os arquivos com o modulo requests você poderá salvar a página web em um arquivo em seu
disco rígido com a função open() e o método write() padrões. Porém há algumas pequenas diferenças. Inicialmente,
você deve abrir o arquivo em modo de escrita binária passando a string 'wb' como segundo argumento de open(). Mesmo
que a pagina esteja em formato de texto simples, será necessário gravar dados binários em vez de dados em formato
texto para preservar a codificação Unicode do texto.
"""
import requests

#Chame requests.get() para fazer download do arquivo.
res = requests.get('https://automatetheboringstuff.com/files/rj.txt')

#Chame raise_for_status() para ver se existem problemas no download
res.raise_for_status()

#Chame open() com 'wb' para criar um novo arquivo em modo de escrita binária.
playFile = open('RomeuEJulieta.txt', 'wb')


"""
O método iter_content() retorna “porções” do conteúdo a cada iteração pelo loop. Cada porção tem o tipo de dado bytes 
e é possível especificar quantos bytes cada porção terá. Cem mil bytes (1 MB) geralmente é um bom tamanho, portanto passe 
100000 como argumento para iter_content() .
"""
#Crie um loop com o método iter_content() do objeto Response .
for i in res.iter_content(100000):
    #Chame write() a cada iteração para gravar o conteúdo no arquivo.
    playFile.write(i)

#Chame close() para fechar o arquivo.
playFile.close()