"""
Página 325 Automatize tarefas maçantes com python
webbrowser Vem junto com o Python e abre um navegador em uma página específica.
"""
#importar webbrowser
import webbrowser

#A função open() do módulo webbrowser pode iniciar um novo navegador em um URL especificado.
webbrowser.open('https://www.google.com')