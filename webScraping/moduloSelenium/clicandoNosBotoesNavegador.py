"""
O Selenium também é capaz de simular cliques em diversos botões do navegador por meio dos métodos a seguir:

browser.back() Clica no botão Back (Retornar).
browser.forward() Clica no botão Forward (Avançar).
browser.refresh() Clica no botão Refresh/Reload (Atualizar/Recarregar).
browser.quit() Clica no botão Close Window (Fechar janela).
"""
from selenium import webdriver

#primeiro criamos a variavel webdriver
navegador = webdriver.Chrome()

#abrimos o site que desejamos inspecionar
navegador.get('https://www.globo.com')

#procuramos especificamente por 'ge' para ser buscado
linkElem = navegador.find_element_by_link_text('ge')

#Clicamos em 'ge'
linkElem.click()

#Retorna a pagina inicial
navegador.back()