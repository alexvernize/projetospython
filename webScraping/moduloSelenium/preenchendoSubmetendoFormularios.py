"""
pagina 360 automatize tarefas macantes

Enviar teclas aos campos de texto em uma página web é uma questão de encontrar o elemento <input> ou <textarea> para
esse campo de texto e chamar o método send_keys(). Veja abaixo um exemplo.
"""
from selenium import webdriver
import time

navegador = webdriver.Firefox()

navegador.get('https://login.yahoo.com')

#procuramos pelo id login-username para ser buscado
emailElem = navegador.find_element_by_id('login-username')

#Envio do email correspondente
emailElem.send_keys('not_my_real_email')

#Botao de next
botaoNext = navegador.find_element_by_id('login-signin')

#Clicar no botao
botaoNext.click()

#Temporizador para dar tempo de carregar a pagina
time.sleep(5)

#Busca pelo id login-passwd
passwordElem = navegador.find_element_by_id('login-passwd')

#Envio da senha correspondente
passwordElem.send_keys('12345')

#Botao de next
botaoNext = navegador.find_element_by_id('login-signin')

#Clicar no botao
botaoNext.click()

#Teoricamente essa funcao abaixo envia as credenciais, porem para esse exemplo nao foi necessaria.
#Envio das credenciais para o login
#passwordElem.submit()