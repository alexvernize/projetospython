"""
O módulo selenium permite que o Python controle diretamente o navegador ao clicar em links e preencher informações de
login por meio de programação, quase como se houvesse um ser humano interagindo com a página. O Selenium permite
interagir com páginas web de uma maneira muito mais sofisticada que o Requests e o Beautiful Soup; contudo, pelo fato
de iniciar um navegador web, ele é um pouco mais lento e difícil de ser executado em background se, por exemplo, for
necessário fazer download de apenas alguns arquivos da Web.
"""

#Iniciando um navegador controlado pelo Selenium
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

binary = FirefoxBinary(‘C:\\Program Files\\Mozilla Firefox\\firefox.exe’)

#quando webdriver.Firefox() é chamado, o navegador web Firefox é iniciado.
navegador = webdriver.Firefox(firefox_binary=binary, executable_path=r’C:\\geckodriver.exe’)

#type() com o valor de webdriver.Firefox() mostra que ele é do tipo WebDriver.
print(type(navegador))

#Além disso, navegador.get('site') chamar direciona o navegador para o site passado
navegador.get('http://www.globo.com')

"""
O Selenium pode fazer muito mais do que as funções descritas aqui. Ele pode modificar os cookies de seu navegador, 
capturar imagens de tela das páginas web e executar JavaScript personalizado. Para saber mais sobre esses recursos, 
acesse a documentação do Selenium em http://selenium-python.readthedocs.org/.
"""
