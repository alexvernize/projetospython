"""
Pagina 359 automatize tarefas macantes

Os objetos WebElement retornados pelos métodos find_element_* e find_elements_* têm um método click() que simula um
clique de mouse nesse elemento. Esse método pode ser usado para seguir um link, fazer uma seleção em um botão de rádio,
clicar em um botão Submit (Submeter) ou disparar qualquer evento que possa ocorrer quando esse elemento for clicado pelo
mouse. Veja um exemplo abaixo.
"""
from selenium import webdriver
import time
import pyautogui

# Primeiro, criamos a variável webdriver
navegador = webdriver.Chrome()

# Abrimos o site que desejamos inspecionar
navegador.get('https://www.globo.com')

# Procuramos especificamente por 'ge' para ser buscado
# linkElem = navegador.find_element_by_link_text('ge')

# Procuramos o elemento usando XPath
xpath = '/html/body/header/div[2]/div[4]/div[2]/ul/li[2]/a'
linkElem = navegador.find_element_by_xpath(xpath)

# Obtemos as coordenadas do elemento
x, y = linkElem.location['x'], linkElem.location['y']

# Aqui é só para mostrar o que o linkElem é
# print(type(linkElem))

# Clicamos em 'ge'
linkElem.click()

# Destaca visualmente o local do clique por 2 segundos
pyautogui.click(x, y)
#time.sleep(2)
input('Pressione Enter para fechar o navegador...')

# Fechamos o navegador
navegador.quit()