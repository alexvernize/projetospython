"""
pagina 361 automatize tarefas macantes

O Selenium tem um módulo para teclas que são impossíveis de digitar em um valor de string que funciona de modo muito
semelhante aos caracteres de escape. Esses valores são armazenados em atributos no módulo selenium.webdriver.common.keys.
Como esse é um nome de módulo bem extenso, será muito mais fácil executar from selenium.webdriver.common.keys
import Keys no início de seu programa; se fizer isso, você poderásimplesmente escrever Keys em qualquer lugar que
devesse normalmente escrever selenium.webdriver.common.keys.
"""
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

#primeiro criamos a variavel webdriver
navegador = webdriver.Firefox()

#abrimos o site que desejamos inspecionar
navegador.get('https://www.globo.com')

htmlElem = navegador.find_element_by_tag_name('html')

#Faz rolagens para o final
htmlElem.send_keys(Keys.END)

#Faz rolagens para o início
#htmlElem.send_keys(Keys.HOME)

"""
A tag <html> é a tag básica dos arquivos HTML: o conteúdo completo do arquivo HTML é inserido entre tags 
<html> e </html>. Chamar browser.find_element_by_tag_name('html') é um bom lugar para enviar teclas para
a página web em geral. Isso será útil, por exemplo, se um novo conteúdo for carregado depois que você tiver 
feito rolagens para o final da página.
"""