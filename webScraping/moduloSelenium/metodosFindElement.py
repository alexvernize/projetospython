"""
Pag 357 automatize tarefas macantes

Os objetos WebDriver têm alguns métodos para localizar elementos em uma página. Eles são divididos em métodos
find_element_* e find_elements_*. Os métodos find_element_* retornam um único objeto WebElement que representa o
primeiro elemento da página que corresponda à sua consulta. Os métodos find_elements_* retornam uma lista de objetos
WebElement_* contendo todos os elementos correspondentes da página. Veja um exemplo abaixo.
"""
from selenium import webdriver
browser = webdriver.Firefox()
browser.get('https://www.globo.com')
try:
    elem = browser.find_element_by_class_name('hui-premium-manchete')
    print('Encontrado <%s> elemento com esse nome de classe!' % (elem.tag_name))
except:
    print('Nao foi possivel encontrar um elemento com esse nome.')

"""
Nesse caso, abrimos o Firefox e o direcionamos a um URL. Nessa página, tentamos encontrar elementos com nome de classe 
igual a 'hui-premium-manchete' e, caso esse elemento seja encontrado, exibimos seu nome de tag usando o atributo
tag_name. Se um elemento desse tipo não for encontrado, exibimos uma mensagem diferente.
"""