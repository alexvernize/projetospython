"""
Pg 341 automatize tarefas macantes

A função bs4.BeautifulSoup() deve ser chamada com uma string contendo o HTML em que o parse será feito. Essa função
retorna um objeto BeautifulSoup.
"""
import requests, bs4

#requests.get() para fazer download da página principal do site da No Starch.
res = requests.get('http://nostarch.com')

#Faz a verificacao se o download ocorreu corretamente
res.raise_for_status()

#passa o atributo text da resposta para bs4.BeautifulSoup(). O objeto BeautifulSoup retornado é armazenado em uma
#variável chamada noStarchSoup.
noStarchSoup = bs4.BeautifulSoup(res.text, features="html5lib")

#Mostra a classe que ele pertence
print("Esse eh o exemplo de um arquivo com requests")
print(type(noStarchSoup))
print('\n')

#Você também pode carregar um arquivo HTML de seu disco rígido passando um objeto File para bs4.BeautifulSoup().

exemploArquivo = open('exemplo.html')
exemploSoup = bs4.BeautifulSoup(exemploArquivo, features="html5lib")

print("Esse eh o exemplo de um html no disco rigido!")
print(type(exemploSoup))

#Depois que tiver um objeto BeautifulSoup, você poderá usar seus métodos para localizar partes específicas de um
# documento HTML.
