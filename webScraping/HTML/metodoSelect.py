"""
pag 342 automatize tarefas macantes

Podemos obter um elemento de uma página web a partir de um objeto BeautifulSoup chamando o método select() e passando
uma string com um seletor CSS para o elemento que estamos procurando. Os seletores são como expressões regulares: eles
especificam um padrão a ser procurado – nesse caso, em páginas HTML em vez de strings de texto genéricas.
"""

print('Esse código extrairá o elemento com id="author" de nosso HTML de exemplo.')
import bs4

arquivoExemplo = open("exemplo.html")

exemploSoup = bs4.BeautifulSoup(arquivoExemplo.read(), features="html5lib")

#Usamos select('#author') para retornar uma lista contendo todos os elementos com id="author".
elems = exemploSoup.select("#author")

#Armazenamos essa lista de objetos Tag na variável elems e len(elems) nos informa que há um objeto Tag na lista, ou
# seja, houve uma correspondência.
print(type(elems))
print(len(elems))
print(type(elems[0]))

#Chamar getText() no elemento fará o texto desse elemento ser retornado, isto é, o HTML interno.
print(elems[0].getText())

#Passar o elemento para str() fará uma string ser retornada com as tags de abertura e de fechamento e o texto do
# elemento.
print(str(elems[0]))

#attrs nos fornece um dicionário com o atributo 'id' do elemento e o valor desse atributo, ou seja, 'author'.
print(elems[0].attrs)

"""
Podemos também extrair todos os elementos <p> do objeto BeautifulSoup.
"""
print('\n Exemplo mostrando a busca por elementos <p> no script HTML')
#Usamos select('p') para buscar as correspondencias com tag <p>
pElems = exemploSoup.select('p')

#Com len() descobrimos quantas correspondencias tivemos
print(len(pElems))

#Passar o elemento para str() fará uma string ser retornada com as tags de abertura e de fechamento e o texto do
# elemento, ja getText() no elemento fará o texto desse elemento ser retornado, isto é, o HTML interno.
print(str(pElems[0]))
print(pElems[0].getText())
print(str(pElems[1]))
print(pElems[1].getText())
print(str(pElems[2]))
print(pElems[2].getText())


