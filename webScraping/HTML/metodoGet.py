"""
Pagina 344 automatize tarefas macantes

O método get() de objetos Tag facilita acessar valores de atributos de um elemento. O método recebe uma string contendo
um nome de atributo e retorna o valor desse atributo. Veja um exemplo abaixo.
"""
import bs4

soup = bs4.BeautifulSoup(open('exemplo.html'), features="html5lib")
spanElem = soup.select('span')[0]
print(str(spanElem))
print(spanElem.get('id'))
print(spanElem.get('some_nonexistent_addr') == None)
print(spanElem.attrs)

"""
Nesse caso, usamos select() para encontrar todos os elementos <span> e então armazenamos o primeiro elemento 
correspondente em spanElem. Passar o nome de atributo 'id' para get() faz o valor do atributo, ou seja, 'author', ser
retornado.
"""

