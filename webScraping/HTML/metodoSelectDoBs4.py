"""
Pagina 343 automatize

O método select() retornará uma lista de objetos Tag , que é o modo como o Beautiful Soup representa um elemento HTML.
A lista conterá um objeto Tag para cada correspondência feita no HTML do objeto BeautifulSoup. Os valores de tag podem
ser passados para a função str() para que as tags HTML que representam possam ser mostradas. Os valores de tag também
têm um atributo attrs que mostra todos os atributos HTML da tag na forma de um dicionário.
"""
import bs4

arquivoExemplo = open('exemplo.html')
exemploSoup = bs4.BeautifulSoup(arquivoExemplo.read())
elems = exemploSoup.select('#autor')
print(type(elems))
print(len(elems))
print(type(elems[0]))
print(elems[0].getText())
print(str(elems[0]))
print(elems[0].attrs)
