"""
Página 329 automatize tarefas maçantes

Para ver mais detalhes sobre o modulo requests acesse http://requests.readthedocs.org/

O módulo requests permite fazer facilmente o download de arquivos da Web sem se preocupar com problemas complicados
como erros de rede, problemas de conexão e compressão de dados. O módulo requests não vem com o Python, portanto será
necessário instalá-lo antes. Na linha de comando, execute pip install requests.
"""
import requests

#A função requests.get() aceita uma string contendo um URL para download.
res = requests.get('https://automatetheboringstuff.com/files/rj.txt')
type(res)

#Podemos dizer que a solicitação dessa página web foi bem-sucedida verificando o atributo status_code do objeto
#Response. Se o seu valor for igual a requests.codes.ok, entãotudo correu bem.
print(res.status_code == requests.codes.ok)

#Aqui estamos vendo o número de caracteres que essa string possui
print(len(res.text))

#Aqui definimos quantos caracteres queremos exibir
print(res.text[:100])

"""
Pg 331

Verificando se houve erros
O objeto Response tem um atributo status_code que pode ser comparado a requests.codes.ok para testar se o download 
foi bem-sucedido. Uma maneira mais simples de verificar se houve sucesso consiste em chamar o método raise_for_status() 
no objeto Response .
"""
print('\n\nVerificando erros')
res = requests.get('http://inventwithpython.com/page_that_does_not_exist')
try:
    res.raise_for_status()
except Exception as exc:
    print('Existe um problema: %s'%(exc))

"""
Sempre chame raise_for_status() após chamar requests.get(). Você deve garantir que o download realmente seja 
bem-sucedido antes que seu programa continue.
"""