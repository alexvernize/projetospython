"""
Página 303 Automatize tarefas maçantes

Obtendo o traceback como uma string

Quando encontra um erro, o Python gera um baú de tesouros chamado traceback contendo informações de erro. O traceback
inclui a mensagem de erro, o número da linha que provocou o erro e a sequência de chamadas de função que resultou
no erro. Essa sequência de chamadas se chama pilha de chamadas (call stack).
"""

#def spam():
#    bacon()

#def bacon():
#    raise Exception('Isso é a mensagem de erro.')
#spam()

"""
O traceback é exibido pelo Python sempre que uma exceção gerada não é tratada. Porém também podemos obtê-lo como 
uma string chamando traceback.format_exc().
"""

'''
Podemos escrever um programa que grave as informações de traceback em um arquivo de log mantendo o programa executando,
dessa forma poderemos olhar o arquivo de log posteriormente.
'''

#Primeiramente importamos traceback
import traceback

#Criamos as instruções try e except
try:
    raise Exception('Isso é uma mensagem de erro.')
except:
    #criando o arquivo que vamos enviar as mensagens de log
    errorFile = open('errorInfo', 'w')
    errorFile.write(traceback.format_exc())
    errorFile.close()
    print('As informações de traceback foram escritas em errorInfo.txt')