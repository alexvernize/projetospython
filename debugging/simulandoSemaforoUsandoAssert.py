"""
Página 306 Automatize tarefas maçantes

Suponha que você esteja criando um programa de simulação de semáforo. A estrutura de dados que representa os semáforos
em uma intersecção é um dicionário com chaves 'ns' e 'lo' para os semáforos voltados na direção norte-sul e leste-oeste,
respectivamente. Os valores dessas chaves serão as strings 'green', '​yellow' ou 'red'.
"""

market_2nd = {'ns': 'green', 'ew': 'red'}
mission_16th = {'ns': 'red', 'ew': 'green'}

def switchLights(stoplight):
    for key in stoplight.keys():
        if stoplight[key] == 'green':
            stoplight[key] = 'yellow'
        elif stoplight[key] == 'yellow':
            stoplight[key] = 'red'
        elif stoplight[key] == 'red':
            stoplight[key] = 'green'
        assert 'red' in stoplight.values(), 'Neither light is red! ' + str(stoplight)
switchLights(market_2nd)

"""
Esse código possui um erro em que os carros virtuais poderiam colidir, já que teriam períodos em que nenhum dos 
cruzamentos teria sinal vermelho, então podemos adicionar um assert para ajudar nessa tarefa.
"""
