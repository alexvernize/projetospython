"""
O python possui as instruções try e except para tratar exceções que podem ser previstas. Também é possível gerar
execeções usando a instrução raise. Gerar uma exceção é uma maneira de dizer “pare de executar o código dessa
função e passe a execução do programa para a instrução except”.
"""

#Para gerar uma exceção usamos a palavra-chave raise seguida da chamada a função Exception() e uma string com uma
#mensagem de erro conveniente.
#raise Exception("Isso é uma mensagem de erro!")

"""
Geralmente, é o código que chama a função, e não a função em si, que sabe como tratar uma exceção. Portanto, 
normalmente, você verá uma instrução raise em uma função e as instruções try e except no código que chama a função.
Veja um exemplo abaixo.
"""

def boxPrint(symbol, width, height):
    if len(symbol) != 1:
        raise Exception('O símbolo deve ser um caracter único de string')
    if width <= 2:
        raise Exception('Width deve ser maior que 2.')
    if height <= 2:
        raise Exception('Height deve ser maior que 2.')

    print(symbol * width)
    for i in range (height - 2):
        print(symbol + (''*(width - 2)) + symbol)
    print(symbol*width)

for sym, w, h in (('*', 4, 4), ('O', 20, 5), ('x', 1, 3), ('ZZ', 3, 3)):
    try:
        boxPrint(sym, w, h)
    except Exception as err:
        print('An exception happened: ' + str(err))