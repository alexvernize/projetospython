"""
Tabela com os níveis de logging na página 311 Automatize tarefas maçantes, veja abaixo algumas delas.
"""
#import logging
import logging
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s - %(message)s')

logging.debug('Some debugging details.')
logging.info('The logging module is working.')
logging.warning('An error message is about to be logged.')
logging.error('An error has occurred.')
logging.critical('The program is unable to recover!')

"""
Página 312 Automatize tarefas maçantes, veja abaixo algumas delas.

Para desabilitar o logging é bastante simples, basta utilizar a função logging.disable(), dessa forma as mensagens de 
log serão ignoradas, coloque sempre no ínicio quando quiser que todas as mensagens de log sejam desabilitadas.
"""


