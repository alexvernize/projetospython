"""
Página 313 automatize tarefas maçantes

Em vez de exibir as mensagens de log na tela, podemos gravá-las em um arquivo-texto. Veja o exemplo abaixo.
"""
import logging

logging.basicConfig(filename='myProgramLog.txt', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

"""
As mensagens de log serão salvadas em myProgramLog.txt. Apesar de serem úteis, as mensagens de logging podem 
congestionar sua tela e dificultar a leitura da saída do programa. Gravar mensagens de logging em um arquivo 
manterá sua tela limpa e fará suas mensagens serem armazenadas para que possam ser lidas após a execução do programa.
"""