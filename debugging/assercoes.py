"""
Página 304 Automatize tarefas maçantes

Uma asserção (assertion) é uma verificação de sanidade para garantir que seu código não está fazendo nada obviamente
incorreto. Essas verificações de sanidade são realizadas por instruções assert.

No código, uma instrução assert é constituída das seguintes partes:

• a palavra-chave assert ;
• uma condição (ou seja, uma expressão avaliada como True ou False );
• uma vírgula;
• uma string a ser exibida quando a condição for False.

"""
podBayDoorStatus = 'open'
assert podBayDoorStatus == 'open', 'The pod bay doors need to be "open".'
podBayDoorStatus = "'I'm sorry, Dave. I'm afraid I can't do that."
assert podBayDoorStatus == 'open', 'The pod bay doors need to be "open".'

"""
Nesse caso, definimos podBayDoorStatus com 'open', portanto, a partir de agora, esperamos que o valor dessa variável 
seja sempre 'open'. Sendo assim, adicionamos uma asserção para garantir que estamos certos em supor que 
podBayDoorStatus seja 'open'. Nesse caso, incluímos a mensagem 'The pod bay doors need to be "open".' 
para que seja mais fácil ver o que há de errado se a asserção falhar.
"""


'''
Página 307

É possível desabilitar as asserções passando a opção -O na execução do python Isso será conveniente quando você tiver 
acabado de escrever e de testar seu programa e não quiser que ele fique lento por realizar verificações de sanidade 
(embora, na maior parte do tempo, as instruções assert não causem uma diferença perceptível de velocidade). As 
asserções devem ser usadas no desenvolvimento, e não no produto final. Quando seu programa for disponibilizado para 
outra pessoa executá-lo, ele deverá estar livre de bugs e não deverá exigir verificações de sanidade.
'''

