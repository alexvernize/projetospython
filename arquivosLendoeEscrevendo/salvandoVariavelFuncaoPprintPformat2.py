""""
Agora que criamos o script com um arquivo .py podemos importar como uma biblioteca do python.
"""

#Importamos o arquivo que criamos anteriormente e usamos como se fosse uma biblioteca com suas funcoes
import meusGatos

print(meusGatos.gatos)
print(meusGatos.gatos[0])
print(meusGatos.gatos[1])

"""
vantagem de criar um arquivo .py (em oposição a salvar variáveis com o módulo shelve ) está no fato de o conteúdo 
do arquivo poder ser lido e modificado por qualquer pessoa que utilize um editor de texto simples, por ele é um 
arquivo-texto. Na maioria das aplicações, porém, salvar dados usando o módulo shelve será a maneira preferida de 
salvar variáveis em um arquivo. Somente tipos de dados básicos como inteiros, números de ponto flutuante, strings, 
listas e dicionários podem ser gravados em um arquivo como texto simples. Objetos File , por exemplo, não podem ser 
codificados como texto.
"""