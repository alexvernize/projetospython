"""
A função pprint.pprint() já foi vista antes, onde pprint fazia uma apresentação elegante e pprint.pformat() retornava
o mesmo texto na forma de uma string ao invés de exibi-lá. Essa leitura volta formatada e também como código python.
Suponha que você tenha um dicionário armazenado em uma variável e queira salvar essa variável e o seu conteúdo para
usar futuramente. A chamada a pprint.pformat() fornecerá uma string que poderá ser gravada em um arquivo .py. Esse
arquivo será seu próprio módulo, que poderá ser importado sempre que você quiser usar as variáveis armazenadas nele.
"""
import pprint

gatos = [{'nome': 'Nuvem', 'cor': 'cinza'}, {'nome': 'Manda Chuva', 'cor': 'branco'}]
pprint.pformat(gatos)

"""
importamos pprint para podermos usar pprint.pformat(). Temos uma lista de dicionários armazenada em uma variável cats. 
Para manter a lista em cats disponível mesmo após termos fechado o shell, utilizamos pprint.pformat() para retorná-la 
como uma string.
"""

#Aqui criamos um arquivo de nome meusGatos.py e colocamos em modo escrita 'w'
arquivo = open("meusGatos.py", 'w')

#Depois usamos o metodo write para escrever no arquivo, repare que usamos o pformat(gatos) para escrever
arquivo.write('gatos = ' + pprint.pformat(gatos) + '\n')

#Por ultimo fechamos o arquivo
arquivo.close()

"""
Ver continuação em 'salvandoVariavelFuncaoPprintPformat2.py'
"""
