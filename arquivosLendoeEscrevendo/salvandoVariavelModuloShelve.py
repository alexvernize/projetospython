"""
É possível salvar variáveis em arquivos shelf binários usando o módulo shelve. Assim, seu programa poderá restaurar
dados em variáveis que estão no disco rígido. O módulo shelve permitirá adicionar funcionalidades Save (Salvar) e
Open (Abrir) em seu programa. Por exemplo, se você executar um programa e inserir alguns parâmetros de configuração,
será possível salvar essas configurações em um arquivo shelf e, em seguida, fazer o programa carregá-las na próxima
vez em que for executado.
"""
#Para utilizar o módulo shelve primeiro faça sua importação
import shelve

#Chame a função open() e passe o nome do arquivo, se ele não existir ainda, será criado
arquivoShelf = shelve.open('meusDados')

#Armazene o valor de shelf retornado em uma variável
gatos = ['Sofia', 'Lulu', 'Branquinho', 'Nuvem']
arquivoShelf['gatos'] = gatos

#Após realizar as modificações chame a função close()
arquivoShelf.close()

"""
Seus programas poderão usar o módulo shelve para reabrir e obter posteriormente os dados desses arquivos shelf. 
Os valores de shelf não precisam ser abertos em modo de leitura ou de escrita – ambas as operações serão permitidas 
após os valores serem abertos.
"""
teste = shelve.open('meusDados')
print(teste['gatos'])
teste.close()

"""
Assim como os dicionários, os valores de shelf têm métodos keys() e values() que retornarão as chaves e os valores 
do shelf em formatos semelhantes a listas. Como esses métodos retornam valores semelhantes a listas, e não listas
de verdade, você deve passá-los à função list() para obtê-los em forma de lista.
"""
teste2 = shelve.open('meusDados')

#Primeiro transformamos em lista e depois passamos o método keys ou values
print(list(teste2.keys()))
print(list(teste2.values()))

"""
O formato texto simples é útil para criar arquivos que serão lidos em um editor de texto como o Notepad ou o 
TextEdit, porém, se quiser salvar dados de seus programas Python, utilize o módulo shelve.
"""
