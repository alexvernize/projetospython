"""
Um arquivo tem duas propriedades fundamentais: um nome de arquivo e um path. O path especifica a localização
de um arquivo no computador. Observe que, enquanto os nomes das pastas e dos arquivos não fazem distinção
entre letras maiúsculas e minúsculas no Windows e no OS X, essa diferença existe no Linux. Outra coisa a ser
notada é que no Windows, os paths são escritos com barras invertidas (\) como separador entre os nomes das
pastas. No OS X e no Linux, porém, utilize a barra para frente (/) como separador de path.
"""

#Utilizando a função os.path.join() evitamos o uso da barra para patchs e podemos usar o programa em qualquer S.O.
#Para chamar a função importamos a biblioteca os
import os
print(os.path.join('home', 'alex', 'Documents', 'projetosphyton', 'programasSimples'))


#O exemplo a seguir une os nomes de uma lista de nomes de arquivo no final do nome de uma pasta:
meusArquivos = ['teste.txt']

for filename in meusArquivos:
    print(os.path.join('/home/alex', filename))

#É possível obter o diretório de trabalho atual como um valor de string usando a função os.getcwd() e alterá-lo
# com os.chdir().
print(os.getcwd())

#print(os.chdir("/home/alex/)")
#print(os.getcwd())