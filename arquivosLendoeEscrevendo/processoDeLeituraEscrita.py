"""
Em Python, há três passos para ler e escrever em arquivos:

1. Chamar a função open() para que um objeto File seja retornado.
2. Chamar o método read() ou write() no objeto File .
3. Fechar o arquivo chamando o método close() no objeto File .

"""
#A função open() abre o arquivo que desejamos modificar
arquivo = open("/home/alex/Documents/testePython/teste.txt")

#Para ler o conteúdo do arquivo usamos a função read()
texto = arquivo.read()
print(texto)

#Se você pensar no conteúdo de um arquivo como um único valor de string extenso, o método read() retornará a
#string armazenada no arquivo.De modo alternativo, podemos utilizar o método readlines() para obter uma
#lista de valores de string do arquivo, uma string para cada linha de texto.
texto2 = open('/home/alex/Documents/testePython/teste2.txt')
print(texto2.readlines())

"""
Não é possível escrever em um arquivo aberto em modo leitura. Para escrever nos arquivos precisamos estar em 
modo de “escrita de texto simples” ou de “adição de texto simples”, ou seja, em modo de escrita (write mode) 
e em modo de adição (append mode), para sermos mais concisos.
"""

#Para abrir o modo de escrita, passe como segundo argumento 'w' na função open()
#Lembrando que se não existir nenhum arquivo com esse nome o python irá criar.
#Não esquecer que o modo de escrita (write) sobreescreverá o arquivo existente.
texto3 = open('/home/alex/Documents/testePython/teste3.txt', 'w')
#Depois use o comando write para escrever.
texto3.write('Hello World!\n')
#Após ler ou escrever em um arquivo, chame o método close() antes de abrir o arquivo novamente.
texto3.close()


#Para abrir o modo de adição, passe como segundo argumento 'a' na função open().
#O modo de adição diferentemente do modo de escrita, adicionará o texto no final do arquivo existente. Podemos
#pensar nisso como a adição em uma lista que está em uma variável em vez de sobrescrever totalmente a variável.
texto4 = open('/home/alex/Documents/testePython/teste4.txt', 'a')
#Depois use o comando write para escrever.
#O método write() diferentemente do modo print() não acrescenta \n automaticamente.
texto4.write('Teste Olá Mundo!\n')
#Após ler ou escrever em um arquivo, chame o método close() antes de abrir o arquivo novamente.
texto4.close()

#Vamos criar uma variável para ler o texto
lerTexto = open('/home/alex/Documents/testePython/teste4.txt')
printarTexto = lerTexto.read()
lerTexto.close()
print(printarTexto)