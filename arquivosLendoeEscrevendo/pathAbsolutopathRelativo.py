"""
Há duas maneiras de especificar um path de arquivo:

• Um path absoluto, que sempre começa com a pasta-raiz.
• Um path relativo, que é relativo ao diretório de trabalho atual do programa.

Temos também as pastas ponto ( . ) e ponto-ponto ( .. ). Essas não são pastas de verdade, mas nomes especiais
que podem ser usados em um path. Um único ponto para um nome de pasta é a forma abreviada de “este diretório”.
Dois pontos (ponto-ponto) quer dizer “a pasta pai”.
Por exemplo:

Diretórios                           Paths relativos                        Paths absolutos
c:                                   ..\                                    C:\
    bacon                            .\                                     C:\bacon
        fizz                         .\fizz                                 C:\bacon\fizz
            spam.txt                 .\fizz\spam.txt                        C:\bacon\fizz\spam.txt
    spam.txt                         .\spam.txt                             C:\bacon\spam.txt
    eggs                             ..\eggs                                C:\eggs
        spam.txt                     ..\eggs\spam.txt                       C:\eggs\spam.txt
    spam.txt                         ..\spam.txt                            C:\spam.txt

O módulo os.path disponibiliza funções que retornam o path absoluto de um path relativo e para verificar se um
dado path representa um path absoluto.
"""
import os

#os.path.abspath(path) retornará uma string com o path absoluto referente ao argumento.
print(os.path.abspath('.'))

#os.path.isabs(path) retornará True se o argumento for um path absoluto e False se for um path relativo.
#Aqui passamos o argumento '.' referente a esse path
print(os.path.isabs('.'))
#Aqui passamos o abspath('.') como argumento, nos dando o path absoluto
print(os.path.isabs(os.path.abspath('.')))

#os.path.relpath(path, início) retornará uma string contendo um path relativo ao path 'início' para 'path'.
#Se início não for especificado, o diretório de trabalho atual será usado como path de início.
#os.path.realpath()

#os.path.dirname(path) retornará uma string contendo tudo que estiver antes da última barra no argumento path.
#os.path.basename(path) retornará uma string contendo tudo que estiver após a última barra no argumento path.
path = '/home/alex/Documents/projetospython'
print(os.path.basename(path))
print(os.path.dirname(path))

#Se o nome de diretório e o nome base de um path forem necessários ao mesmo tempo, podemos usar basename() e
# dirname() juntos
print((os.path.dirname(path), os.path.basename(path)))

# você também poderia simplesmente chamar os.path.split() para obter um valor de tupla contendo essas duas strings
print(os.path.split(path))

#observe que os.path.split() não recebe um path de arquivo e retorna uma lista de strings com cada pasta. Para isso,
# utilize o método destring split() e separe a string em os.sep . Lembre-se do que vimos anteriormente, que a
# variável os.sep é definida com a barra correta de separação de pastas para o computador que estiver executando
# o programa.

#Esse código retornará as strings pertencentes ao caminho do arquivo, note que a primeira string é vazia e representa
#o '/' do linux em windows a primeira string seria 'C:'
print(path.split(os.path.sep))

# O método de string split() funciona retornando uma lista contendo cada parte do path. Esse método funcionará em
# qualquer sistema operacional se os.path.sep lhe for passado.


