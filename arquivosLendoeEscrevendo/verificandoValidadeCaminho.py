"""
Muitas funções Python falharão gerando um erro se você lhes fornecer um path inexistente. O módulo os.path
disponibiliza funções para verificar se um dado path existe e se é um arquivo ou uma pasta.
"""
import os

#os.path.exists(path) retornará True se o arquivo ou a pasta referenciada no argumento existir e retornará
# False caso contrário.
print(os.path.exists('/home/alex/qualquercoisa/'))
print(os.path.exists('/home/alex/Documents/'))

#os.path.isfile(path) retornará True se o argumento referente ao path existir e for um arquivo e retornará
# False caso contrário.
print(os.path.isfile('/home/alex/Documents/'))
print(os.path.isfile('/home/alex/Documents/ingles.txt'))

#os.path.isdir(path) retornará True se o argumento referente ao path existir e for uma pasta e retornará
# False caso contrário.
print(os.path.isdir('/home/alex/Documents/ingles.txt'))
print(os.path.isdir('/home/alex/Documents/'))

#É Possível verificar se existe um DVD ou um pen drive conectado ao computador utilizando a função os.path.exists().
#Por exemplo, se eu quiser verificar se há um pen drive com um volume chamado D:\ em meu computador Windows, posso
# fazer isso com:
print(os.path.exists("D:\\"))