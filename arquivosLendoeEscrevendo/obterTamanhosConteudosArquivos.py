"""
O módulo os.path disponibiliza funções para obter o tamanho de um arquivo em bytes e os arquivos e as pastas
que estiverem em uma determinada pasta.
"""
import os

#os.path.getsize(path) retornará o tamanho em bytes do arquivo no argumento path.
print(os.path.getsize("/home/alex/PokerStarsInstall.exe"))

#os.listdir(path) retornará uma lista de strings com nomes de arquivo para cada arquivo no argumento path.
#(Observe que essa função está no módulo os, e não em os.path.)
print(os.listdir("/home/alex/Pictures/"))

#Para descobrir o tamanho total de todos os arquivos nesse diretório, é possível usar os.path.getsize()
# e os.listdir() juntos.
tamanhoTotal = 0
for i in os.listdir("/home/alex/Pictures/"):
    tamanhoTotal = tamanhoTotal + os.path.getsize(os.path.join('/home/alex/Pictures/', i))
print(tamanhoTotal)