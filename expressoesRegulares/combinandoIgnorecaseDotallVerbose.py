"""
Infelizmente, a função re.compile() aceita apenas um único valor
como segundo argumento. Podemos contornar essa limitação combinando as
variáveis re.IGNORECASE , re.DOTALL e re.VERBOSE utilizando o caractere
pipe ( | ) que, nesse contexto, é conhecido como o operador ou bit a bit
(bitwise or).
"""
import re

regex = re.compile("teste", re.IGNORECASE | re.DOTALL | re.VERBOSE)
texto = regex.search("teste teste teste teste").group()
print(texto)

#Essa sintaxe é um pouco antiga e tem origem nas primeiras versões do Python.


