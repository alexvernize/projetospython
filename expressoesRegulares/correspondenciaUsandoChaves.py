"""
Se você tiver um grupo que deseja repetir um número específico de vezes, insira um número entre chaves após o grupo
em sua regex. Por exemplo, a regex (Ha){3} corresponde à string 'HaHaHa', mas não a 'HaHa', pois essa última tem
apenas duas repetições do grupo (Ha). Também é possível especificar um intervalo mínimo e um máximo entre vírgulas.
Por exemplo, a regex (Ha) {3,5} corresponde a 'HaHaHa', 'HaHaHaHa' e 'HaHaHaHaHa'.
Essas duas expressões representam a mesma coisa:
(Ha){3,5}
((Ha)(Ha)(Ha))|((Ha)(Ha)(Ha)(Ha))|((Ha)(Ha)(Ha)(Ha)(Ha))
"""

import re

haRegex = re.compile(r"(Ha){3}")
texto = haRegex.search("HaHaHa")
texto2 = haRegex.search("Ha")
print(texto.group())
#Como não há correspondência em 'Ha' , search() retorna None.
print(texto2 == None)

"""
As expressões regulares em Python são greedy (gulosas) por default, o que significa que, em situações ambíguas, 
a correspondência será feita com a maior string possível. Na versão nongreedy (não gulosa) das chaves, que faz
a correspondência com a menor string possível, um ponto de interrogação é usado depois da chave de fechamento.
"""

#Nesse primeiro exemplo o retorno será a string total
greedyHaRegex = re.compile(r"(Ha){3,5}")
greedy = greedyHaRegex.search("HaHaHaHaHa")
print(greedy.group())
#Nesse segundo exemplo adicionamos o ponto de interrogação e o retorno será a string com as primeiras 3 recorrências.
nonGreedyHaRegex = re.compile(r"(Ha){3,5}?")
nonGreedy = nonGreedyHaRegex.search("HaHaHaHaHa")
print(nonGreedy.group())

##################################################################################
#   Observe que o ponto de interrogação pode ter dois significados em expressões #
#regulares: declarar uma correspondência nongreedy ou indicar um grupo opcional. #
#Esses significados não têm nenhuma relação entre si.                            #
##################################################################################