"""
O * (chamado de asterisco) quer dizer “corresponda a zero ou mais” – o grupo que antecede o asterisco pode ocorrer
qualquer número de vezes no texto. Esse grupo poderá estar totalmente ausente ou ser repetido diversas vezes.
"""

import re

batRegex = re.compile(r"Bat(wo)*man")
texto = batRegex.search("As aventuras de Batman")
texto2 = batRegex.search("As aventuras de Batwoman")
texto3 = batRegex.search("As aventuras de Batwowowowowowowoman")
#Para 'Batman' , a parte referente a (wo)* da regex corresponde a zero instâncias de wo na string.
print(texto.group())
#(wo)* corresponde a uma instância de wo.
print(texto2.group())
#para 'Batwowowowoman' ,(wo)* corresponde a sete instâncias de wo.
print(texto3.group())