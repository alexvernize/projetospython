'''
O caractere ? marca o grupo que o antecede como sendo uma parte opcional do padrão.
'''

import re

#A parte da expressão regular que contém (wo)? significa que o padrão wo é um grupo opcional. A regex corresponderá
#a textos que não tenham nenhuma ou que tenham uma instância de wo.
batRegex = re.compile(r'Bat(wo)?man')
texto = batRegex.search("As aventuras de Batman")
texto2 = batRegex.search("As aventuras de Batwoman")
print(texto.group())
print(texto2.group())

#Usando o exemplo acima podemos criar um código que a regex procure números de telefone com ou sem código de área.
numeroRegex = re.compile(r'(\d\d\d-)?\d\d\d\d\d-\d\d\d\d')
texto3 = numeroRegex.search("Meu telefone é 041-99820-3053")
texto4 = numeroRegex.search("Meu telefone é 99820-3053")
print(texto3.group())
print(texto4.group())