"""
O ponto-asterisco corresponderá a qualquer caractere, exceto uma quebra de linha. Ao passar re.DOTALL
como segundo argumento de re.compile(), podemos fazer o caractere ponto corresponder a todos os caracteres,
incluindo o caractere de quebra de linha.
"""
import re

semQuebraDeLinha = re.compile(".*")#Qualquer caractere, exceto quebra de linha
texto = semQuebraDeLinha.search("Logo eu que nem pensava.\n Eu nao imaginava te merecer\n "
                                "e agora sou o dono...")#\n significa quebra de linha

#texto = semQuebraDeLinha.search("") eh possivel colocar o .group() no final da linha depois do search:
texto = semQuebraDeLinha.search("Logo eu que nem pensava.\n Eu nao imaginava te merecer\n "
                                "E agora sou o dono desse amor...").group()
#print(texto.group()) Fazendo o codigo acima nao eh necessario chamar o metodo group de novo no print
print(texto)

comQuebraDeLinha = re.compile(".*", re.DOTALL)#Aqui adicionamos o re.DOTALL, dessa forma a quebra de linha tambem
                                              #entra na regex
texto2 = comQuebraDeLinha.search("Logo eu que nem pensava.\n Eu nao imaginava te merecer\n"
                                 "E agora sou o dono desse amor... ").group()
print(texto2)
