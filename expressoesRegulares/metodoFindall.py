"""
Além do método search(), os objetos Regex também têm um método findall(). Enquanto search() retorna um objeto
Match do primeiro texto correspondente na string pesquisada, o método findall() retorna as strings de todas as
correspondências na string pesquisada.
"""
import re

#Exemplo de método search, apenas um número como rerono
numeroTeleFoneRegex = re.compile(r"\d\d\d-\d\d\d\d\d-\d\d\d\d")
texto = numeroTeleFoneRegex.search("Celular Álex: 041-99958-4512 Celular Day: 041-98556-2425")
print(texto.group())

#Exemplo de método findall, retorna uma lista de strings ao invés de um objeta Match (Desde que não haja grupos
# na expressão regular).
numeroTeleFoneRegex2 = re.compile(r"\d\d\d-\d\d\d\d\d-\d\d\d\d")#não tem nenhum grupo
texto2 = numeroTeleFoneRegex.findall("Celular Álex: 041-99958-4512 Celular Day: 041-98556-2425")
print(texto2)

#Se houver grupos na expressão regular, findall() retornará uma lista de tuplas. *(Grupos estão entre parênteses)
numeroTeleFoneRegex3 = re.compile(r"(\d\d\d)-(\d\d\d\d\d)-(\d\d\d\d)")#tem grupos
texto3 = numeroTeleFoneRegex.findall("Celular Álex: 041-99958-4512 Celular Day: 041-98556-2425")
print(type(texto3))

#Aqui devolveu como lista, não sei porque, o livro diz que deveria ser uma tupla