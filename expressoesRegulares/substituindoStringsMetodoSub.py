"""
As expressões regulares não só podem identificar padrões de texto como
também podem substituir esses padrões por novos textos. O método sub() dos
objetos Regex recebe dois argumentos. O primeiro argumento é uma string
para substituir qualquer correspondência. O segundo é a string para a
expressão regular. O método sub() retorna uma string com as substituições
aplicadas.
"""
import re
nomesRegex = re.compile(r"Dayane \w+")#Nome passado pra regex

"O método sub() recebe dois argumentos, a palavra que quer que substitua a regex e o texto."
texto = nomesRegex.sub('CENSURADO', "A Dayane é mãe do Matheus.")
print(texto)

"""
Às vezes, pode ser necessário utilizar o próprio texto correspondente como
parte da substituição. No primeiro argumento de sub() , podemos digitar \1, \2,
\3 e assim por diante para dizer “insira o texto do grupo 1, 2, 3 e assim por
diante na substituição”. Veja abaixo.
"""

nomesRegex2 = re.compile(r"Agente (\w)\w*")
texto = nomesRegex2.sub(r"\1******", "Hoje os Agentes do FBI estiveram em missão, são eles o Agente Álex, Agente Dayane e "
                        "Agente Matheus.")
print(texto)