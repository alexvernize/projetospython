"""
O símbolo de acento circunflexo ( ^ ) também pode ser usado no início de uma regex para indicar que uma
correspondência deve ocorrer no início de um texto pesquisado. Da mesma maneira, podemos colocar um sinal
de dólar ($) no final da regex para indicar que a string deve terminar com esse padrão de regex. Além disso,
podemos usar ^ e $ juntos para indicar que a string toda deve corresponder à regex – ou seja, não é suficiente
que uma correspondência seja feita com algum subconjunto da string.
"""

import re

inicioComBomDia = re.compile(r"^Bom dia")
teste = inicioComBomDia.search('Bom dia, como você está?')
teste2 = inicioComBomDia.search('Olá , como você está?')
print(inicioComBomDia.search('Bom dia, como você está?'))
print(teste.group())
print(teste2 == None)

#A string r'\d$' de expressão regular corresponde a strings que terminem com um caractere numérico de 0 a 9.
finalComNumero = re.compile(r"\d$")
print(finalComNumero.search("Seu número é 45"))
print(finalComNumero.search("Seu número é o dois") == None)

#A string r'^\d+$' de expressão regular, que possue o sinal de mais '+', corresponde a strings que comecem
#e terminem com um ou mais caracteres #numéricos.
todaStringNumera = re.compile(r"^\d+$")
teste3 = todaStringNumera.search("124578931")
print(teste3.group())