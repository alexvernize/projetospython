"""
Haverá ocasiões em que você vai querer fazer a correspondência de um conjunto de caracteres, porém as classes
abreviadas de caracteres ( \d , \w , \s e assim por diante) serão amplas demais. Você pode definir sua própria
classe de caracteres usando colchetes. Por exemplo, a classe de caracteres [aeiouAEIOU] corresponderá a qualquer
vogal, tanto minúscula quanto maiúscula. Veja um exemplo.
"""
import re

#Para criar uma classe de regex use colchetes e adicione o que deseja que seja buscado, nesse caso todas as vogais
#maiúsculas de minúsculas seráo buscadas.
criandoRegex = re.compile(r"[aeiouAEIOU]")
print(criandoRegex.findall("Essa é uma frase de teste e exemplo."))

#Também é possível incluir intervalos de letras ou de números usando hífen. Por exemplo [a-zA-Z0-9]
criandoRegex2 = re.compile(r"[0-9]")
print(criandoRegex2.findall("Ele disse 15 vezez que isso daria errado eu avisei umas 50"))

"""
Observe que, nos colchetes, os símbolos normais de expressão regular não são interpretados. Isso quer dizer que 
não é necessário escapar os caracteres .. , * , ? ou () com uma barra invertida na frente. Por exemplo, a classe 
de caracteres [0-5.] corresponderá aos dígitos de 0 a 5 e um ponto. Não é preciso escrever essa classe como 
[0-5\.].
"""

# Utilizando o acento circunflexo '^' logo depois do COLCHETE de abertura da classe de caracteres (lembre que aqui
# estamos falando de classes que foram criadas e por isso utilizamos entre colchetes, o acento circunflexo também
# tem outro significado em regex, veja 'acento circunflexo e sinal de dólar nessa pastas de regex'), criamos uma
# classe negativa de caracteres, no caso o software irá buscar tudo o que não tiver esses valores.
consoantesRegex = re.compile(r"[^aeiouAEIOU]")
print(consoantesRegex.findall("Essa é uma frase de teste e exemplo."))



