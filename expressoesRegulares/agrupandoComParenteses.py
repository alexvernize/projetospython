"""
Suponha que você queira separar o código de área do restante do número de
telefone. A adição de parênteses criará grupos na regex: (\d\d\d)-(\d\d\d-
\d\d\d\d) . Então você poderá usar o método group() do objeto decorrespondência
para obter o texto correspondente de apenas um grupo.
"""
import re

numeroCelular = re.compile(r"(\d\d\d)-(\d\d\d\d\d-\d\d\d\d)")
telefone = numeroCelular.search('Meu celular é 041-99599-6365.')

#group(1) será o primeiro conjunto entre parêntesesda string, no caso aqui o DDD
print('Aqui o DDD: ' + telefone.group(1))

#group(2) será o segundo conjunto entre parênteses da string, no caso o telefone
print('Aqui o número do telefone: ' + telefone.group(2))

#vazio ou 0 'group() ou group(0)' será o regex completo
print('Aqui o DDD mais o número do telefone: ' + telefone.group())

#Se desejar obter todos os grupos de uma só vez, utilize o método groups() –
#observe a forma do plural no nome. Dessa forma será retornado uma tupla
print('Aqui o DDD mais o número como uma tupla: ' + str(telefone.groups()))

#Como telefone.groups() retorna uma tupla com diversos valores, podemos usar o
#truque da atribuição múltipla para atribuir cada valor a uma variável
#diferente, como na linha abaixo.
codigoDeArea, numeroPrincipalCelular = telefone.groups()
print(codigoDeArea)
print(numeroPrincipalCelular)

#Os parênteses têm um significado especial em expressões regulares, porém se
#for necessário utilizar parênteses para separar, por exemplo, o código de área,
#devemos usar o caracter de escape. Veja abaixo.
dddComParenteses = re.compile(r"(\(\d\d\d\))-(\d\d\d\d\d-\d\d\d\d)")
numeroNovo = dddComParenteses.search('O celular da Lulu é 041-98854-3325.')

print(numeroNovo.group(1))
print(numeroNovo.group(2))
print(numeroNovo.group())

