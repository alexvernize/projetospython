'''
As expressões regulares (regular expressions), chamadas de regexes por
questões de concisão, correspondem a descrições para um padrão de texto.
Por exemplo, um \d é uma regex que representa um dígito – ou seja, qualquer
numeral único entre 0 e 9. Por exemplo um número de celular com DDD poderia
ser \d\d\d-\d\d\d\d\d-\d\d\d\d. É possível digitar de outra forma mais limpa
esse código, por exemplo podemos acrescentar o valor repetido entre chaves,
o código do celular ficaria dessa forma \d{3}-\d{5}-\d{4}.
'''

#Para usar todas as funções regex precisamos importar o módulo correspondente
import re

'''
*****************
*Dica importarte*
*****************
**********************************************************************************
*Lembre-se de que os caracteres de escape em Python usam a barra invertida (\).  *
*Entretanto, ao colocar um r antes do primeiro caractere de aspas do valor da    *
*string, podemos marcar a string como pura (raw string), que não considera       *
*caracteres de escape. Como as expressões regulares geralmente utilizam barras   *
*invertidas, é conveniente passar strings puras para a função re.compile() em vez*
*de digitar barras invertidas extras. Digitar r'\d\d\d-\d\d\d-\d\d\d\d' é muito  *
*mais fácil do que digitar '\\d\\d\\d-\\d\\d\\d-\\d\\d\\d\\d'.                   *
**********************************************************************************
'''

#re.compiler() cria o objeto regex usando uma string pura.
numeroCelular = re.compile(r'\d\d\d-\d\d\d\d\d-\d\d\d\d')

#O método search() de um objeto regex pesquisa a string recebida em busca de qualquer
#correspondência com a regex. Se nenhum padrão regex for encontrado o método search()
#retornará 'None', do contrário retornará um objeto Match.
telefones = numeroCelular.search("Meu celular é 041-99820-3053.")

#Objetos Match têm um método group() que retornará o texto correspondente extraído
#da string pesquisada.
print('Número de celular encontrado: ' + telefones.group())
