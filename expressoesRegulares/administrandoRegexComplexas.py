"""
As expressões regulares serão convenientes se o padrão de texto para a
correspondência for simples. Porém fazer a correspondência de padrões
complicados de texto pode exigir expressões regulares longas e confusas.
Podemos atenuar esse problema dizendo à função re.compile() que ignore
espaços em branco e comentários na string de expressão regular. Esse “modo
verbose” pode ser habilitado se a variável re.VERBOSE for passada como
segundo argumento de re.compile().
"""

#Esse codigo abaixo:
#phoneRegex = re.compile(r'((\d{3}|\(\d{3}\))?(\s|-|\.)?\d{3}(\s|-|\.)\d{4}(\s*(ext|x|ext.)\s*\d{2,5})?)')

#Poderia ser substituido dessa forma:

phoneRegex = re.compile(r'''(
(\d{3}|\(\d{3}\))?
# código de área
(\s|-|\.)?
# separador
\d{3}
# primeiros 3 dígitos
(\s|-|\.)
# separador
\d{4}
# últimos 4 dígitos(\s*(ext|x|ext.)\s*\d{2,5})? # extensão
)''', re.VERBOSE)#VERBOSE sendo passado

#Dessa forma fica muito mais facil de entendet e legivel o texto.