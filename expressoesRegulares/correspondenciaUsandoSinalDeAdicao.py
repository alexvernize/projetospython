"""
Enquanto * quer dizer “corresponda a zero ou mais”, o + (ou sinal de adição) quer dizer “corresponda a um ou mais”.
De modo diferente do asterisco, que não exige que seu grupo esteja presente na string correspondente, o grupo que
antecede um sinal de adição deve aparecer pelo menos uma vez.
"""

import re

batRegex = re.compile(r"Bat(wo)+man")
texto = batRegex.search("As aventuras de Batman")
texto2 = batRegex.search("As aventuras de Batwoman")
texto3 = batRegex.search("As aventuras de Batwowowowoman")
#Como ele não possue um Match e estamos passando o método group() o código abaixo dará um erro AttributeError
#print(texto.group())
print(texto == None)
print(texto2.group())
print(texto3.group())
