'''
O caractere pipe '|' pode ser utilizado para fazer a correspondência de uma entre várias expressões. Por
exemplo, a expressão regular r'Matheus|Alex' corresponde a 'Matheus' ou a 'Alex'.
'''

import re

#Quando acontecer de a string pesquisada conter os dois valores, a primeira ocorrência será retornada como
#objeto Match.
regex = re.compile(r'Matheus|Alex')
texto = regex.search(r'O Matheus é filho do Alex.')
texto2 = regex.search(r'O Alex é pai do Matheus.')
print(texto.group())
print(texto2.group())
#É possível encontrar todas as ocorrências com o método findall()

#O pipe também pode ser usado para fazer a correspondência de um entre diversos padrões como parte de sua regex.
#Se quiser por exemplo buscar batman, batmovel, batcaverna, etc, pode ser especificado o prefixo para a busca.
#Veja abaixo.
batRegex = re.compile(r'bat(man|movel|caverna|aviao)')
texto3 = batRegex.search('O batmovel é rápido.')
print(texto3.group())
print(texto3.group(1))