"""
Normalmente, as expressões regulares fazem correspondência de textos com
o tipo exato de letra, ou seja, maiúscula ou minúscula, que você especificar.

regex1 = re.compile("Alex")
regex1 = re.compile("ALex")
regex1 = re.compile("AlEx")
regex1 = re.compile("AleX")
regex1 = re.compile("ALEx")

Sao todas diferentes entre si.
"""
import re

"""
Para fazer sua regex ignorar as diferenças entre letras maiúsculas
e minúsculas (ser case-insensitive), re.IGNORECASE ou re.I pode ser passado
como segundo argumento de re.compile(). Veja abaixo.
"""

regex1 = re.compile(r"ALEX", re.I)"Repare que passamos o nome todo em maiúsculo"

texto = regex1.search("O Alex é pai do Matheus.").group()
print(texto)"Ele retorna o nome Alex que não está todo em maiúsculo"