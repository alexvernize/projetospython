"""
Às vezes, vamos querer fazer uma correspondência de tudo. Por exemplo, suponha que você queira fazer a
correspondência da string 'Nome:' seguida de qualquer texto e 'Sobrenome:' seguida de qualquer
texto tambem. Podemos usar ponto-asterisco ( .* ) para indicar “qualquer caractere”. Lembre-se
de que o caractere ponto quer dizer “qualquer caractere único, exceto a quebra de linha” e o caractere
asterisco quer dizer “zero ou mais ocorrências do caractere anterior”.
"""
import re

regexNomes = re.compile(r"Nome: (.*) Sobrenome: (.*)")

texto = regexNomes.search("Nome: Matheus Sobrenome: Prado")
print(texto.group(1))
print(texto.group(2))
print(texto.groups())

"""
O ponto-asterisco utiliza o modo greedy: ele sempre tentará fazer a correspondência do máximo de texto 
possível. Para corresponder a todo e qualquer texto em modo nongreedy, utilize ponto, asterisco e ponto de
interrogação ( .*? ).
"""

regexNonGreedy = re.compile(r"<.*?>")#Colocando o '?' deixamos em modo greedy
texto2 = regexNonGreedy.search('<Sou desse chao> onde o rei e peao.>')#Repare que apos o '>' ele nao pega
print(texto2.group())

regexGreedy = re.compile(r'<.*>')
texto3 = regexGreedy.search('<Sou desse chao> onde o rei e peao.>')
print(texto3.group())

"""
Ambas as regexes, de modo geral, podem ser traduzidas como “faça a
correspondência de um sinal de ‘menor que’ seguido de qualquer caractere
seguido de um sinal de ‘maior que’”. Porém a string '<To serve man> for
dinner.>' tem duas correspondências possíveis para o sinal de ‘maior que’. Na
versão nongreedy da regex, o Python faz a correspondência com a menor
string possível: '<To serve man>' . Na versão greedy, o Python faz a
correspondência com a maior string possível: '<To serve man> for dinner.>'.
"""