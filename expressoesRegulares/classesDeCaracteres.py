"""
\d pode representar qualquer dígito, ou seja, \d é uma versão abreviada da expressão regular (0|1|2|3|4|5|6|7|8|9).
Há várias dessas classes abreviadas de caracteres, veja algumas abaixo.

\d Qualquer dígito de 0 a 9.
\D Qualquer caractere que não seja um dígito de 0 a 9.
\w Qualquer letra, dígito ou o caractere underscore. (Pense nisso como
uma correspondência de caracteres de “palavra”.)
\W Qualquer caractere que não seja uma letra, um dígito ou o caractere
underscore.
\s Qualquer espaço, tabulação ou caractere de quebra de linha. (Pense
nisso como uma correspondência de caracteres de “espaço”.)
\S Qualquer caractere que não seja um espaço, uma tabulação ou uma
quebra de linha.
"""
import re

#Aqui colocamos o '\d+' que significa 1 dígito ou mais, depois '\s' que significa um caractere de espaço e por fim
#'\w+' que si
regex = re.compile(r"\d+\s\w+")
#O único texto que ele não vai devolver é o 1kg de açúcar porque o depois do número não vem o espaço e sim uma letra.
print(regex.findall("15 uvas, 16 laranjas, 8 batatas, 5 cenouras, 12 ovos, 1kg de açúcar"))