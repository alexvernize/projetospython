"""
O caractere . (ou ponto) em uma expressão regular é chamado de caractere-curinga e corresponde a qualquer
caractere, exceto uma quebra de linha.
"""
import re
#O caractere '.' equivale apenas a um caracter.
regex = re.compile(r".at")
print(regex.findall("The cat in the hat sat on the flat mat."))

"""
Lembre-se de que o caractere ponto corresponderá somente a um caractere, motivo pelo qual a correspondência 
para a palavra flat no exemplo anterior foi feita somente com lat .
"""
