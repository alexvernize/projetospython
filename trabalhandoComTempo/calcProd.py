'''
Página 461 livro automatize tarefas maçantes com python
'''

import time 

#Função que calcula o produto dos primeiros cem mil números
def calcProd():
  #Calcula o produto dos 1000000 primeiros números.
  produto = 1
  for i in range(1, 100000):
    produto = produto*i
  return produto

#Marca a hora que iniciou o cálculo, utilizando a função time.time()
startTime = time.time()

#Chama a função calcProd
prod = calcProd()

#Termina os produtos e chama novamente a função time.time() agora salva na variável endTime
endTime = time.time()

print('O resultado é %s.' %(len(str(prod))))
print('O tempo em segundos para calcular foi de %s.' %(endTime - startTime))
