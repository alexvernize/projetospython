# encoding: utf-8


"""
    A função input permite ao usuário digitar alguma instrução,
    por exemplo valores de variáveis que queiram utilizar no 
    software. A sintaxe funciona da seguinte maneira, primeiro
    utilizamos a palavra input seguida por parênteses input().
    Como na função print ou qualquer outra instrução, não é necessário
    o uso do ";" ao final da instrução.
"""

print("Software para cálculo de área de um retângulo:")
print("______________________________________________\n")
base = int(input("Digite o valor da base: "))
altura = int(input("Digite o valor da altura: "))

area = int(base * altura)

print(area,"m²")
