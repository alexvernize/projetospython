#Algumas formas de utilizar a função range()

#Forma padrão, utilizando apenas um número entre parênteses.
#Nesse formato o número de iterações, será o número entre os 
#parênteses, começando sempre pelo número 0.
for i in range(5):
    print(i)

#Colocando dois números entre os parenteses, ele irá começar 
#pelo primeiro número e terminar um antes do último, nesse caso
#abaixo: 12, 13, 14, 15.
for j in range (12, 16):
    print(j)

#Colocando três números ele irá começar pelo primeiro, terminar 
#antes do último utilizar o último número como valor de somar ou 
#subtrair os numerais, por exemplo (0, 10, 2), começa com o primeiro
#e segue de dois em dois até o último número possível, nesse caso o 
#8: 0, 2, 4, 6, 8. Também é possível utilizar números negativos para 
#andar ao contrário (5, -1, -1): 5, 4, 3, 2, 1.
for x in range(5, -1, -1):
    print(x)
