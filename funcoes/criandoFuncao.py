#Além das diversas funções que o python disponibiliza é possível 
#criar suas próprias funções. Uma função é como um miniprograma
#dentro de um programa.

#A sintaxe da criação é a seguinte, digite a palavra chave 'def'
#e após de um nome a sua função com os parenteses e depois dois 
#pontos, veja abaixo como fazer.
def Ola():
    print('Olá')
    print('Oi')
    print('E aí')

#Para chamar sua função em python, digite o nome dela e depois os 
#parenteses.

Ola()

