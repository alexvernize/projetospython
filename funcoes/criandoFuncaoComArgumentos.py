#É possível passar argumentos por funções, o exemplo abaixo
#mostra isso em funcionamento.

def hello(name):
    print('Hello ' + name)

hello('Alex')
hello('Juarez')

