# encoding: utf-8

#Em python3 é necessário o uso to tipo de codificação, sempre colocar
#na primeira linha do software em python. 

"""A função print em python é bastante simples de ser utilizada
   utiliza apenas a palavra print seguida de parênteses e dentro
   desses parênteses é colocado o que deseja que seja mostrado na 
   tela, seja um valor de variável, ou apenas um texto."""

#Em python, diferentemente de diversas linguagens, não é necessário 
#o uso do ";" ao final da instrução, isso acaba facilitando muito.
#Tomar cuidado com a identação em python, o software poderá dar erros
#se a identação estiver errada, por exemplo colocar espaço no início
#dessa função print abaixo.
print("Testando a função print do python")
