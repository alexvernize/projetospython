'''
O método remove() recebe o valor a ser removido da lista em que 
é chamada, veja abaixo.
'''

praia = ['Areia', 'Prancha', 'Praia', 'Biquini']
print('lista = ', praia)

praia.remove('Biquini')
print('lista = ', praia)

#Se existir valores iguais, somente o primeiro valor será excluído.

churrasco = ['Picanha', 'Costela', 'Cupim', 'Alcatra', 'Costela', 'Linguiça', 'Costela']
print('churrasco = ', churrasco)
churrasco.remove('Costela')
print('churrasco = ', churrasco)
