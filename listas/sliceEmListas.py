# coding: utf-8

'''
Assim como um índice pode acessar um único valor de uma lista, um slice
pode obter diversos valores de uma lista na forma de uma nova lista.
Veja o exemplo abaixo.
'''

lista = ['Brinquedo', 'Bola', 'Boneco', 'Pião', 'Bicicleta', 'Lego']

#Para fazer um slice de uma lista, utilizamos ':' entre os índices, com o 
#primeiro valor de índice significando qual o primeiro valor que desejamos
#e o segundo valor é o índice que o slice termina, sem incluir ele.
print(lista)
print('Este é um exemplo de slice: ', end='')
print(lista[1:3])

#É possível omitir os valores sem os especificar. Deixar de especificar o 
#primeiro índice é o mesmo que deixar o valor de '0', ou seja no início da
#lista. Deixar de especificar o segundo índice é o mesmo que usar o tamanho
#todo da lista e deixar de utilizar os dois índices, utilizando somente o 
#':', é o mesmo que utilizar a lista toda.
print('Este é um exemplo de uso sem o primeiro índice: ', end='')
print(lista[:3])
print('Este é um exemplo de uso sem o segundo índice: ', end='')
print(lista[1:])
print('Este é um exemplo de uso sem ambos os índices: ', end='')
print(lista[:])


