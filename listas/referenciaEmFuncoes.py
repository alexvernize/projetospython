'''
As referências são particularmente importantes para entender como os
argumentos são passados às funções. Quando uma função é chamada, os
valores dos argumentos são copiados para as variáveis referentes aos
parâmetros. No caso das listas (e dos dicionários, que serão descritos no
próximo capítulo), isso quer dizer que uma cópia da referência será usada
para o parâmetro.
'''

def carro(algumParametro):
    algumParametro.append('Retrovisor')

veiculo = ['Rodas', 'Volante', 'Capô', 'Bancos']
print('veiculo = ', veiculo)
carro(veiculo)
print('veiculo = ', veiculo)

