'''
Listas não são os únicos tipos de dados que representam seuqências
ordenadas de valores. Por exemplo, as strings e as listas, na realidade, são
semelhantes se considerarmos uma string como uma “lista” de caracteres
textuais únicos. Várias das ações que podemos realizar em listas, também 
podemos realizar em strings: indexação, slicing, uso em loops for com
len() e uso de operadores in e not in. Veja abaixo alguns exemplos.
'''

nome = 'Alex'
print(nome)
print(nome[0])
print(nome[-2])
print(nome[0:2])
print('Al' in nome)

for i in nome:
    print('***' + i + '***')


