'''
Quando atribuímos um valor a uma variável e depois colocamos em outra variável
esses valores podem ser mudados de uma para a outra separadamente. Em listas 
isso não acontece. o atribuir uma lista a uma variável, na verdade, você estará 
atribuindo uma referência de lista à variável. Uma referência é um valor que 
aponta para uma porção de dados, euma referência de lista é um valor que aponta 
para uma lista. Veja os exemplos abaixo para ficar mais claro.
'''

a = 10
print('a = ', a)
b = a
print('b = ', b)
a = 100
#Repare que o valor de 'a' é alterado, mas não influencia o valor de 'b', apesar 
#de ter colocado 'b = a'
print('a = ', a, 'b = ', b)

lista = ['Alex', 'Day', 'Matheus']
print('lista = ', lista)
lista2 = lista
print('lista2 = ', lista2)

#Aqui alteramos o valor na 'lista' de 'Alex' para 'Alberto', repare que o valor da 
#'lista2' também é alterado.
lista[0] = 'Alberto'
print('lista = ', lista)
print('lista2 = ', lista2)


