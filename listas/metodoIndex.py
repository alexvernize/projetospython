'''
O método index() serve para retornar o índice de um valor que estiver
presente na lista, se não existir esse valor o python apresentará um 
erro: ValueError. Veja abaixo o método em funcionamento.
'''

lista1 = ['Casa', 'Quarto', 'Sala', 'Cozinha']
print('lista1 = ', lista1)
print('Casa tem o índice de: ', lista1.index('Casa'))

#Se existir um valor duplicado na lista, o primero valor de índice será 
#retornado, como abaixo.

lista2 = ['Computador', 'TV', 'Telefone', 'Fogão', 'TV']
print('lista2 = ', lista2)
print ('O índice que primeiro aparece é o que o método retorna: ', lista2.index('TV'))
