'''
A função copy() criar uma duplicata de um valor mutável como uma lista ou 
um dicionário e não apenas uma cópia de referência, essa função tem a sintaxe
de copy.copy(), já a função deepcopy() serve para copiar listas que contém 
outras listas, a sintaxe é a copy.deepcopy(). 
'''

import copy

lista = ['Alex', 'Dayane', 'Matheus', 'Pedro']
print('lista = ', lista)
copia = copy.copy(lista)
print('copia = ', copia)

copia[2] = 'Miguel'
print('lista = ', lista)
print('copia = ', copia)

#Repare que é possível alterar o valor da cópia sem mudar o de lista, o que
#mostra que não estamos trabalhando com a referência e sim com outra lista
#em outra referência.
