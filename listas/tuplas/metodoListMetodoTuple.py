'''
Assim como str(42) retorna '42' , que é a representação em string do inteiro 42 ,as funções list() e tuple() retornarão as versões de lista e de tupla dos valores
passados a elas.
'''

animais = ('cachorro', 'gato', 'pato')
print('animais = ', animais)
animais2 = ['cachorro', 'gato', 'pato']
print('animais2 = ', animais2)

print('animais = ', list(animais))
print('animais2 = ', tuple(animais2))

