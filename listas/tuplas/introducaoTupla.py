'''
O tipo de dado tupla (tuple) é quase idêntico ao tipo de dado lista, exceto em
relação a dois aspectos. Em primeiro lugar, as tuplas são digitadas com
parênteses, isto é, ( e ) no lugar de colchetes. Em segundo lugar as tuplas são 
imutáveis assim como as strings. 

casa = ('Sala', 'Quarto', 'Banheiro', 'Cozinha')
casa[0] = 'Lavanderia'

Esse código acima causa um erro porque tuplas não são mutáveis.
'''

casa = ('Sala', 'Quarto', 'Banheiro', 'Cozinha')

print('casa = ', casa)
print('casa = ', casa[1])
print('casa = ', casa[1:3])
print('casa = ', len(casa))

#Se existir apenas um valor em sua tupla, é necessário colocar uma vírgula no final
#dessa forma o python saberá que está trabalhando com uma tupla.

tupla = ('Alex',)
tupla2 = ('Alex')
print(type(tupla))
print(type(tupla2))
