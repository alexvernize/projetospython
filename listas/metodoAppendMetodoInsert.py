'''
Para adicionar valores a listas utilize os métodos append() ou
o método insert(). Esses são métodos de lista e não podem ser 
usados por outros valores como por exemplo strings ou inteiros,
algo como:

a = 42 
a.insert(1, 'teste')

Esse código acima causa um erro em python. Veja abaixo o funcionamento
correto dos métodos.
'''

lista = ['Uva', 'Laranja', 'Maça', 'Pera']
print('lista = ', lista)

#Repare que para usar o método utilizamos o nome da lista '.' e 
#append, depois os parênteses e dentro o valor que deseja adicionar
#entre aspas simples.
lista.append('Abacaxi')
print('lista = ', lista)

#O método append sempre adiciona o valor no final da lista, já o 
#método insert() pode inserir um valor em qualquer índice que 
#desejar já que ele funciona com 2 argumentos, veja abaixo o exemplo.

lista2 = ['Ronaldo', 'Romário', 'Rivaldo', 'Ricardo']
print('lista2 = ', lista2)

lista2.insert(2, 'Roberto')
print('lista2 = ', lista2)


