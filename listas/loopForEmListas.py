'''
Os códigos abaixo tem a mesma função e fazem a mesma coisa
a diferença principal é que um usa a função range e o outro
uma lista:

for i in range(4):
	print(i)

for i in [0, 1, 2, 3]:
	print(i)
'''

lista1 = ['Prova', 'Penal', 'Caderno']

'''
Usar range(len(lista)) no loop for é prático porque o código
do loop pode acessar o índice (como a variável i) e o valor
nesse índice (como lista[i]).
'''
for i in range(len(lista1)):
	print('Numero ' + str(int(i)+1) + ' tem na escola: ' + lista1[i])
