#encoding: utf-8

'''
Uma lista é um valor que contém diversos valores em sequência ordenada.
'''

#listas são criadas entre colchetes e separadas por vírgula. Strings ficam
#entre aspas simples e números de forma normal.

lista = ['gato', 'cachorro', 'coelho', 1, 45, 1.5]

print(lista)

#para imprimir na tela um valor individual de uma lista, utilizamos um índice
#entre colchetes '[0]', os valores das listas sempre iniciam em 0.

print('Esse é o primeiro argumento da lista: ' + lista[0])
print('Esse é o segundo argumento da lista: ' + lista[1])
print('Esse é o terceiro argumento da lista: ' + lista[2]) 
#Não esquecer que inteiros não podem ser concatenados, então transforme em string.
print('Esse é o quarto argumento da lista: ' + str(lista[3]))

#Não é possível utilizar um índice como ponto flutuante, conforme exemplo abaixo.
#print('Esse exemplo ocorre um erro: ' + lista[1.0])

#Porém é possível transformar em int para isso.
print('Exemplo transformando o índice de float para int: ' + str(lista[int(4.0)]))

#Listas podem conter outros valores de listas, essas listas de listas podem ser 
#acessadas usando índice múltiplos, como no exemplo abaixo.

listaComLista = [['Alex', 'Julia', 'Luana', 'Raquel'],[1, 3, 4, 7, 1.5]]

print('Usando o índice [0] apenas: ', end='')
print(listaComLista[0])
print('Usando o índice [1] apenas: ', end='')
print(listaComLista[1])
print('Primeiro valor da primeira lista: ', end='')
print(listaComLista[0][0])
print('Primeiro valor da segunda lista: ', end='')
print(listaComLista[1][0])

#É possível utilizar índices negativos, e eles funcionam do último pro primeiro
#o -1 é o último valor, o -2 o penúltimo e assim por diante.

indiceNegativo = ['Tv', 'Carro', 'Celular']
print(indiceNegativo)

print('Usando o índice negativo de -1: ' + indiceNegativo[-1])

