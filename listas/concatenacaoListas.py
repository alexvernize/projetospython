'''
É possível concatenar listas, o operador + pode combinar duas listas
para criar um novo valor de lista. O operador + também pode ser usado 
com uma lista e um valor inteiro para repetir a lista.
'''

lista1 = ['Banana', 'Maça', 'Abacaxi', 'Uva']

lista2 = ['Tomate', 'Alface', 'Cebola']

lista3 = lista1 + lista2
print('lista1 = ', end='')
print(lista1)
print('lista2 = ', end='')
print(lista2)
print('Aqui temos a soma das listas: ', end='')
print(lista3)

#Também é possível multiplicar os valores das listas, como no exemplo abaixo.

lista4 = lista1*3
print('Multiplicando a lista1 três vezes: ', end='')
print(lista4)
