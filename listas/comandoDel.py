'''
O comando del serve para excluir um valor de determinado índice em 
uma lista. Veja abaixo.
'''

lista = ['Futebol', 'Volei', 'Basquete', 'Handebol']
print('esportes = ', lista)

del lista[2]

print('esportes = ', lista)
