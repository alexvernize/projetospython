'''
É possível alterar valores em uma lista usando os índices, o exemplo
abaixo mostra isso em execução
'''

lista = ['mouse', 'Laptop', 'Monitor', 'Processador']

print('Essa é a lista original: ', end='')
print(lista)

lista [1] = 'HD'

print('Essa é a lista alterando o elemento [1]: ', end='')
print(lista)
