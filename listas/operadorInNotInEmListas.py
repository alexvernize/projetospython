'''
Os operadores in e not in podem determinar se um valor está 
ou não está em uma lista. Essas expressões serão avaliadas 
como um valor booleano.
'''

meusAnimais = ['Floquinho', 'Spike', 'Totó']

print('Digite um nome de pet: ')
nome = input()
if nome not in meusAnimais:
    print('Não tenho esse nome nos meus pets ' + nome + '.')
else:
    print(nome + ' é meu pet.')
