'''
Para remover valores em listas utilizamos a instrução 'del', dessa forma
é possível remover valores através dos índices. Veja abaixo o funcionamento
'''

lista1 = ['Palhaço', 'Elefante', 'Leão', 'Malabaristas']

print(lista1)

del lista1[0]

print('Utilizando o comando del no primeiro elemento: ', end='')
print(lista1)
