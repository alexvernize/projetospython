'''
A atribuição múltipla permite atribuir valores de uma lista a 
diversas variáveis em uma única linha de código.
Para atribuir uma variável a um valor de lista, utilizamos o 
nome que quer de varíavel o sinal de '=' e o nome da lista com 
o índice que deseja colocar a variável, algo como isso
tamanho = lista[0], repare que primeiro vem o nome da variável 
que você deseja nomear e depois o nome da lista. O exemplo de 
código abaixo mostra isso usando a múltipla atribuição.
'''

lista1 = ['Carro', 'Volkswagen', 'Preto', 'Gol']

'''
Repare em duas coisas bem importantes, a primeira que primeiro 
é digitado o nome das variáveis, depois o '=' e o nome da lista.
A segunda coisa a prestar atenção é que a quantidade de variáveis 
deve ser exatamente igual ao tamanho da lista.
'''
veiculo, marca, cor, modelo = lista1

print(lista1)
print('Primeiro item da lista nomeado como veiculo e printado: ', end='')
print(veiculo)


