'''
As listas de valores numéricos ou de strings podem ser ordenadas com 
o método sort(). Não é possível ordenar listas que contenham valores
numéricos e valores do tipo string simultaneamente, por exemplo a lista 
teste = [10, 'Alex', 4, '10', 'Lucia', 'Lais'], o python não saberá 
comparar esse valores. Outra coisa importante é que o método sort()
utiliza a tabela ASCII em vez da ordem alfabética para ordenar strings, 
então a letra 'a' minúscula vem ordenada depois da letra 'Z' maiúscula.
Veja abaixo alguns exemplos do uso do método sort().
'''

numeros = [10, 2, 9, 1, 87, -7, -1]
print('numeros = ', numeros)
numeros.sort()
print('numerosCrescentes = ', numeros)

animais = ['Leão', 'Anta', 'Tamanduá', 'Girafa', 'Papagaio']
print('animais = ', animais)
animais.sort()
print('animaisDeAaZ = ', animais)

'''
Também é possível ordenar os valores em ordem inversa, para isso 
utilizamos como argumento no método sort() 'reverse=True'. Veja 
abaixo.
'''

numeros.sort(reverse=True)
print('numerosDecrescentes = ', numeros)

animais.sort(reverse=True)
print('animaisDeZaA = ', animais)

'''
Não esquecer que o método sort usa ASCII, então as letras de A a Z 
maiúsculas virão antes das letras a a z minúsculas.
'''
letras = ['Luiza', 'Alex', 'Julio', 'evandro', 'alessandro', 'valdir']
print('letras = ', letras)
letras.sort()
print('letras = ', letras)

'''
Existe uma forma de ordenar em forma alfabética normal passando o 
argumento key com o str.lower na chamada do método sort(). Esse argumento
trata os itens como se fossem todos em letras minúsculas, sem alterar os 
valores da lista. Veja abaixo.
'''

letras.sort(key=str.lower)
print('letras = ', letras)
