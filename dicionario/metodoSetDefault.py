'''
O método setdefault() defini um valor em um dicionário para uma chave 
que ainda não exista. O método recebe dois argumentos onde o primeiro
é a chave a ser verificada (se não existir ele adiciona ao dicionário),
o segundo argumento é o valor dessa chave. 

É possível fazer isso de outra forma, mas com algumas linhas a mais, veja

pessoa = {'nome': 'Matheus', 'idade': 5}

if 'peso' not in pessoa:
    pessoa['peso'] = 10

Veja abaixo como fazer isso com o método setdefault()
'''

pessoa = {'nome': 'Alex', 'idade': 29, 'sexo': 'masculino'}

print('pessoa = ', pessoa)

pessoa.setdefault('peso', 78)

print('pessoa = ', pessoa)

