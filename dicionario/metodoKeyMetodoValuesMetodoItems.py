'''
Há três métodos de dicionário que retornam valores semelhantes a listas
contendo as chaves, os valores ou ambos – ou seja, as chaves e os valores –
do dicionário: keys(), values() e items(). Os valores retornados por esses
métodos não são listas de verdade: eles não podem ser modificados e não têm
um método append(). Porém esses tipos de dados ( dict_keys, dict_values e
dict_items, respectivamente) podem ser usados em loops for. Veja o exemplo 
abaixo
'''
dicionario = {'cor': 'azul', 'numero': 10, 'objeto': 'TV'}

print('Método values()')
for i in dicionario.values():
    print(i)

print('\nMétodo keys()')
for j in dicionario.keys():
    print(j)

print('\nMétodo items()')
for k in dicionario.items():
    print(k)

print('\nAtribuição múltipla')
for l, m in dicionario.items():
    print('Key:' + l + ' Valor:' + str(m))
