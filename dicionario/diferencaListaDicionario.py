'''
Uma das principais diferenças entre dicionários e listas é que
uma lista é ordenada, onde o primeiro ítem da lista será o '0'
lista[0], já os dicionários não possuem um primeiro item porque 
não são ordenadas. Veja o exemplo abaixo.
'''

lista = ['gato', 'cachorro', 'coelho']
lista2 = ['cachorro', 'coelho', 'gato']

print(lista == lista2)

dicionario = {'nome': 'Spike', 'idade': '7', 'animal': 'cachorro'}
dicionario2 = {'idade': '7', 'animal': 'cachorro', 'nome': 'Spike'}

print(dicionario == dicionario2)
