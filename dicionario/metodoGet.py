'''
O método get() serve para evitar um erro de caso não exista a chave no 
dicionário ele retorna um valor default. Esse método recebe dois 
argumentos, a chave que deseja procurar e uma valor default para se ela 
não existir. Veja abaixo o funcionamento do método.
'''

viagem = {'camisetas': 5, 'calças': 7, 'tênis': 3}

print('Eu vou levar para a viagem ' + str(viagem.get('camisetas', 8)) + ' camisetas')
print('Eu vou levar para a viagem ' + str(viagem.get('luvas', 0)) + ' luvas')
