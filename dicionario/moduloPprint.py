'''
O módulo pprint possui algumas funções úteis para a apresentação
do dicionário. Dentre elas temos a pprint() e a pformat(), que 
farão uma apresentação elegante (pretty print) dos valores do 
dicionário. Isso será conveniente quando quisermos uma apresentação 
mais limpa dos itens de um dicionário em comparação com o que é 
proporcionado por print().
'''

import pprint

mensagem = "Pau no cu de quem tá lendo!"

contador = {}

for i in mensagem:
        contador.setdefault(i, 0)
        contador[i] = contador[i] + 1

pprint.pprint(contador)

print(pprint.pformat(contador))
