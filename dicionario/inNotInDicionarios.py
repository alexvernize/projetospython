'''
Os operadores in e not in podem verificar valores tanto em listas quanto
em dicionários. O uso é o mesmo que para listas, veja abaixo.
'''

dicionario = {'nome': 'Alex', 'idade': 29, 'peso': 78}

print('nome' in dicionario.keys())

print('Alex' in dicionario.values())

print('cor' in dicionario.keys())

#Observe que abaixo não é utilizado nenhum método, pois é uma versão mais 
#concisa de keys()
print('teste' in dicionario)
