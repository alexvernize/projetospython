'''
Assim como uma lista, um dicionário é uma coleção de diversos valores, 
a diferença é que os índices dos dicionários podem utilizar vários 
tipos de dados diferentes, e não apenas inteiros. Outra coisa é que em 
dicionários utilizamos as chaves '{}' e não os colchetes '[]' como em 
listas e nem parênteses '()' como em tuplas. Os índices nos dicionários 
são chamados de chaves (keys), e uma chave juntamente com seu valor 
associado é chamada de par chave-valor (key-value pair). Veja um exemplo 
abaixo.
'''

#Repare na sintaxe que utilizamos primeiro a keys depois ':' e após o valor.
meuCarro = {'tamanho': 'pequeno', 'cor': 'azul', 'marca': 'Ford'}

#Outra coisa importante é que assim como as listas, para utilizar os índices
#os colocamos entre colchetes '[]'
print('Meu Carro é', meuCarro['cor'])

#É possível utilizar inteiro nas keys também
dicionarioTeste = {10: 'teste'}
print(dicionarioTeste[10])
