'''
À medida que modelar objetos mais complexos, você perceberá que precisará
de dicionários e listas que contenham outros dicionários e listas. Tanto 
listas quanto dicionários são possíveis aninhar, ou seja, criar um dicionário
dentro de um dicionário. Veja um exemplo abaixo.
'''

#Esse programa mostra o que cada pessoa está trazendo para o lanche

todosOsConvidados = {'Alex':{'maçãs': 5, 'bananas': 10}, 'Dayane': {'refrigerantes': 5,
    'bolos': 3}, 'Luiza': {'sanduíches': 8, 'tortas': 5}}

def totalDeCoisas(convidados, item):
    lanchesTrouxe = 0
    for i, j in convidados.items():
        lanchesTrouxe += j.get(item, 0)
    return lanchesTrouxe

print('Número de coisas que trouxe: ')
print(' - Maçãs: ' + str(totalDeCoisas(todosOsConvidados, 'maçãs')))
print(' - Bananas: ' + str(totalDeCoisas(todosOsConvidados, 'bananas')))
print(' - Refrigerantes: ' + str(totalDeCoisas(todosOsConvidados, 'refrigerantes')))
print(' - Bolos: ' + str(totalDeCoisas(todosOsConvidados, 'bolos')))
print(' - Sanduíches: ' + str(totalDeCoisas(todosOsConvidados, 'sanduíches')))
print(' - Tortas: ' + str(totalDeCoisas(todosOsConvidados, 'tortas')))
