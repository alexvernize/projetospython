import pandas as pd

# Carregar dados
dividends = pd.read_csv("petrobras_dividendos.csv", index_col="Date", parse_dates=True)

# Calcular métricas de interesse
total_dividends = dividends['Dividends'].sum()
average_dividend_yield = dividends['Dividends'].mean()

# Imprimir resultados
print(f"Total de Dividendos: {total_dividends}")
print(f"Rendimento Médio: {average_dividend_yield}")