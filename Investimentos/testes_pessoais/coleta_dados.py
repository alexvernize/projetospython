import yfinance as yf

# Baixar dados históricos de dividendos da Petrobras
petrobras = yf.Ticker("PETR4.SA")
dividends = petrobras.dividends

# Salvar os dados em um arquivo CSV
dividends.to_csv("petrobras_dividendos.csv")