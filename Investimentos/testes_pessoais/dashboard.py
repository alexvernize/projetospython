from dash import Dash, dcc, html
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd

# Carregar dados
dividends = pd.read_csv("petrobras_dividendos.csv", index_col="Date", parse_dates=True)

# Iniciar o aplicativo Dash
app = Dash(__name__)

# Layout do dashboard
app.layout = html.Div(children=[
    html.H1(children='Análise de Dividendos da Petrobras'),

    dcc.Graph(
        id='dividend-chart',
        figure=px.line(dividends, x=dividends.index, y='Dividends', title='Histórico de Dividendos')
    )
])

# Rodar o aplicativo
if __name__ == '__main__':
    app.run_server(debug=True)
