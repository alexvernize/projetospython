from selenium import webdriver

def obter_valor_fii_selenium(ativo):
    try:
        # Transforma o nome do ativo para minúsculas
        ativo_lower = ativo.lower()

        # Construir a URL com o ativo fornecido
        url = f'https://investidor10.com.br/fiis/{ativo_lower}'

        # Configurar o driver do Selenium (certifique-se de ter o WebDriver correspondente instalado)
        driver = webdriver.Chrome()  # ou utilize outro driver, como Firefox, Edge, etc.

        # Abrir a página
        driver.get(url)

        # Encontrar o elemento span com a classe "valor" dentro da seção com id "cards-ticker"
        valor_element = driver.find_element_by_css_selector('#cards-ticker span.valor')

        # Obter o valor do elemento
        valor = valor_element.text.strip()

        # Imprimir as informações
        print(f"Valor do FII ({ativo}): {valor}")

    except Exception as e:
        print(f"Erro ao obter informações para {ativo}: {e}")

    finally:
        # Fechar o navegador após o uso
        driver.quit()

# Obter ativo do usuário
ativo = input("Digite o código do FII (por exemplo, MXRF11): ")

# Chamar a função para obter informações do FII usando Selenium
obter_valor_fii_selenium(ativo)
