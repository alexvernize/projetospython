import pandas as pd
import yfinance as yf

# Lista de tickers de FIIs (exemplo)
fiis_tickers = ['HGLG11.SA', 'XPML11.SA', 'VISC11.SA']

# Criar um DataFrame vazio para armazenar os dados
df = pd.DataFrame(columns=['Ticker', 'Valor', 'P/VP', 'Tipo', 'DY', 'Quantidade', 'Valor Total'])

# Função para obter informações do FII
def obter_informacoes_fii(ticker, quantidade):
    try:
        fii = yf.Ticker(ticker)

        # Obter informações financeiras
        info = fii.info

        # Calcular valor total e adicionar informações ao DataFrame
        preco_fii = info.get('regularMarketPrice', 0)
        valor_total = preco_fii * quantidade
        df.loc[len(df)] = [ticker, preco_fii,
                           info.get('priceToBook', 'N/A'), info.get('sector', 'N/A'),
                           info.get('trailingAnnualDividendYield', 'N/A'),
                           quantidade, valor_total]

        print(f"Informações obtidas para {ticker}")

    except Exception as e:
        print(f"Erro ao obter informações para {ticker}: {e}")

# Iterar sobre os tickers e obter informações
for ticker in fiis_tickers:
    quantidade = float(input(f"Quantidade de cotas a comprar para {ticker}: "))
    obter_informacoes_fii(ticker, quantidade)

# Calcular porcentagem por tipo de ativo
porcentagem_por_tipo = df.groupby('Tipo')['Valor Total'].sum() / df['Valor Total'].sum() * 100

# Salvar o DataFrame em uma planilha Excel
df.to_excel('informacoes_fiis.xlsx', index=False)

# Imprimir porcentagem por tipo de ativo
print("Porcentagem por Tipo de Ativo:")
print(porcentagem_por_tipo)
