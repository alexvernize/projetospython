import pandas as pd
import requests
from bs4 import BeautifulSoup

# Lista de tickers de FIIs (exemplo)
fiis_tickers = ['HGLG11', 'XPML11', 'VISC11']

# Criar um DataFrame vazio para armazenar os dados
df = pd.DataFrame(columns=['Ticker', 'Valor', 'P/VP', 'Tipo', 'DY', 'Quantidade', 'Valor Total'])

# Função para obter informações do FII usando scraping no site StatusInvest
def obter_informacoes_fii(ticker, quantidade):
    try:
        url = f'https://statusinvest.com.br/fundos-imobiliarios/{ticker}'
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')

        # Encontrar as informações desejadas no HTML do site
        preco_fii = float(soup.find('div', {'class': 'value'}).text.replace('R$', '').replace(',', '.').strip())
        pvp = soup.find('span', {'class': 'value'}).text.strip()
        setor = soup.find('div', {'class': 'fund-info-list'}).find_all('span', {'class': 'value'})[0].text.strip()
        dy = soup.find('div', {'class': 'fund-info-list'}).find_all('span', {'class': 'value'})[1].text.strip()

        # Calcular valor total e adicionar informações ao DataFrame
        valor_total = preco_fii * quantidade
        df.loc[len(df)] = [ticker, preco_fii, pvp, setor, dy, quantidade, valor_total]

        print(f"Informações obtidas para {ticker}")

    except Exception as e:
        print(f"Erro ao obter informações para {ticker}: {e}")

# Iterar sobre os tickers e obter informações
for ticker in fiis_tickers:
    quantidade = float(input(f"Quantidade de cotas a comprar para {ticker}: "))
    obter_informacoes_fii(ticker, quantidade)

# Calcular porcentagem por tipo de ativo
porcentagem_por_tipo = df.groupby('Tipo')['Valor Total'].sum() / df['Valor Total'].sum() * 100

# Salvar o DataFrame em uma planilha Excel
df.to_excel('informacoes_fiis.xlsx', index=False)

# Imprimir porcentagem por tipo de ativo
print("Porcentagem por Tipo de Ativo:")
print(porcentagem_por_tipo)
