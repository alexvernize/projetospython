import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import numpy as np
import pandas as pd
import pandas_datareader.data as web
import yfinance as yf
import warnings

# Desativar temporariamente FutureWarnings
warnings.simplefilter(action='ignore', category=FutureWarning)

yf.pdr_override()

# Obter dados do Yahoo Finance
ibov = web.get_data_yahoo('^BVSP')#, start='2011-01-01', end='2024-01-19')

# Calcular as médias móveis
ibov['MM21'] = ibov['Close'].rolling(21).mean()
ibov['MM200'] = ibov['Close'].rolling(200).mean()


# Restaurar as configurações originais de aviso
warnings.resetwarnings()

# Criar aplicação Dash
app = dash.Dash(__name__)

# Layout da aplicação
app.layout = html.Div(children=[
    html.H1(children='Pontos IBOV Dashboard'),

    dcc.Graph(
        id='ibov-graph',
        figure={
            'data': [
                {'x': ibov.index, 'y': ibov['Close'], 'type': 'line', 'name': 'IBOV'},
                {'x': ibov.index, 'y': ibov['MM21'], 'type': 'line', 'name': 'MM21'},
                {'x': ibov.index, 'y': ibov['MM200'], 'type': 'line', 'name': 'MM200'},
            ],
            'layout': {
                'title': 'IBOV Closing Prices',
                'xaxis': {'title': 'Date'},
                'yaxis': {'title': 'Close Price'},
            }
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)

ibov["Close"].plot(figsize=(22,8), label="IBOV")
