import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import requests
import time

app = dash.Dash(__name__)

def get_crypto_price(coin_id):
    url = f"https://api.coingecko.com/api/v3/simple/price?ids={coin_id}&vs_currencies=brl"
    response = requests.get(url)
    data = response.json()
    return data[coin_id]['brl']

app.layout = html.Div([
    dcc.Interval(
        id='interval-component',
        interval=60*1000,  # Atualiza a cada minuto (em milissegundos)
        n_intervals=0
    ),
    html.H1("Crypto Price Monitor"),
    dcc.Graph(id='live-update-graph'),
])

@app.callback(Output('live-update-graph', 'figure'),
              [Input('interval-component', 'n_intervals')])
def update_graph_live(n):
    coin_id = "avalanche-2"
    target_price = 185
    price = get_crypto_price(coin_id)
    if price >= target_price:
        message = f"Preço de {coin_id} atingiu R${target_price}! Hora de comprar!"
    else:
        message = f"Preço atual de {coin_id}: R${price}"
    trace = go.Scatter(
        x=[time.strftime('%Y-%m-%d %H:%M:%S')],
        y=[price],
        name='Price',
        mode='lines+markers'
    )
    return {'data': [trace], 'layout': {'title': message}}

if __name__ == '__main__':
    app.run_server(debug=True, use_reloader=False)
