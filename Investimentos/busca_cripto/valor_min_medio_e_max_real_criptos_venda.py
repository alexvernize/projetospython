import requests
import time
from collections import deque

# Defina os valores médios que você pagou por cada criptomoeda em reais
valores_medios = {
    'btc': 214905.83,
    'avax': 170.90,
    'matic': 3.97,
    'eth': 11489.61,
    'sol': 486.83
}

# Defina a porcentagem de lucro desejada (entre 5% e 15%)
porcentagem_lucro_maxima = 0.15
porcentagem_lucro_media = 0.10
porcentagem_lucro_minima = 0.05
janela_media = 10  # Janela para calcular a média móvel ponderada

# Função para obter o preço atual de uma criptomoeda em reais
def obter_preco(criptomoeda):
    url = f'https://api.binance.com/api/v3/ticker/price?symbol={criptomoeda.upper()}BRL'
    response = requests.get(url)
    return float(response.json()['price'])

# Função para calcular a média móvel ponderada dos lucros recentes
def calcular_media_movel(lucros):
    pesos = list(range(1, len(lucros) + 1))
    return sum(l * p for l, p in zip(lucros, pesos)) / sum(pesos)

# Função para verificar se o preço atingiu alguma das metas de lucro
def verificar_meta(criptomoeda, preco_atual, valor_medio, lucro_atual):
    meta_maxima = valor_medio * (1 + porcentagem_lucro_maxima)
    meta_media = valor_medio * (1 + porcentagem_lucro_media)
    meta_minima = valor_medio * (1 + porcentagem_lucro_minima)
    
    if preco_atual >= meta_maxima:
        print(f'{criptomoeda.upper()} atingiu a meta de lucro máxima! Preço atual: R$ {preco_atual:.2f}')
    elif preco_atual >= meta_media:
        print(f'{criptomoeda.upper()} atingiu a meta de lucro média! Preço atual: R$ {preco_atual:.2f}')
    elif preco_atual >= meta_minima:
        print(f'{criptomoeda.upper()} atingiu a meta de lucro mínima! Preço atual: R$ {preco_atual:.2f}')

# Loop principal
lucros_recentes = {criptomoeda: deque(maxlen=janela_media) for criptomoeda in valores_medios}
while True:
    print("\nValores atuais das criptomoedas:")
    for criptomoeda, valor_medio in valores_medios.items():
        preco_atual = obter_preco(criptomoeda)
        lucro_atual = (preco_atual - valor_medio) / valor_medio
        lucros_recentes[criptomoeda].append(lucro_atual)
        
        media_movel = calcular_media_movel(lucros_recentes[criptomoeda])
        print(f'\n{criptomoeda.upper()}: R$ {preco_atual:.2f} - Lucro médio: {media_movel * 100:.2f}%')
        
        verificar_meta(criptomoeda, preco_atual, valor_medio, lucro_atual)
    
    # Aguarde 60 segundos antes de verificar novamente
    time.sleep(60)