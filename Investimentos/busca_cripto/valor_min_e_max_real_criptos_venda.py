import requests
import time

# Defina os valores médios que você pagou por cada criptomoeda em reais
valores_medios = {
    'btc': 214905.83,
    'avax': 170.90,
    'matic': 3.97,
    'eth': 11489.61,
    'sol': 486.83
}

# Defina a porcentagem de lucro desejada (entre 10% e 20%)
porcentagem_lucro = 0.15
porcentagem_minima_lucro = 0.055  # Nova porcentagem mínima de lucro

# Função para obter o preço atual de uma criptomoeda em reais
def obter_preco(criptomoeda):
    url = f'https://api.binance.com/api/v3/ticker/price?symbol={criptomoeda.upper()}BRL'
    response = requests.get(url)
    return float(response.json()['price'])

# Função para verificar se o preço atingiu a meta de lucro
def verificar_meta(criptomoeda, preco_atual, valor_medio, lucro_atual):
    meta_lucro = valor_medio * (1 + porcentagem_lucro)
    meta_minima_lucro = valor_medio * (1 + porcentagem_minima_lucro)  # Nova meta de lucro mínima
    if preco_atual >= meta_lucro:
        print(f'{criptomoeda.upper()} atingiu a meta de lucro! Preço atual: R$ {preco_atual:.2f}')
    elif lucro_atual <= porcentagem_minima_lucro:  # Verifica se o lucro atual está abaixo da porcentagem mínima
        print(f'{criptomoeda.upper()} está em queda de lucro! Preço atual: R$ {preco_atual:.2f}')
        if preco_atual <= meta_minima_lucro:
            print(f'Venda sua {criptomoeda.upper()} agora para garantir lucros!')
            # Adicione aqui o código para vender a criptomoeda, se desejado

# Loop principal
while True:
    print("\nValores atuais das criptomoedas:")
    for criptomoeda, valor_medio in valores_medios.items():
        preco_atual = obter_preco(criptomoeda)
        lucro_atual = (preco_atual - valor_medio) / valor_medio
        print(f'{criptomoeda.upper()}: R$ {preco_atual:.2f}')
        verificar_meta(criptomoeda, preco_atual, valor_medio, lucro_atual)
    # Aguarde 60 segundos antes de verificar novamente
    time.sleep(60)