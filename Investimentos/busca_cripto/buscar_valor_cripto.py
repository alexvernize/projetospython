import requests
import time

def get_crypto_price(coin_id):
    url = f"https://api.coingecko.com/api/v3/simple/price?ids={coin_id}&vs_currencies=brl"
    response = requests.get(url)
    data = response.json()
    return data[coin_id]['brl']

def monitor_crypto_price(coin_id, target_price):
    while True:
        price = get_crypto_price(coin_id)
        print(f"O preço atual de {coin_id} é R${price}")
        if price >= target_price:
            print(f"O preço de {coin_id} atingiu R${target_price}! Hora de comprar!")
            break
        time.sleep(60)  # Verifica a cada minuto

# Exemplo de uso
coin_id = "avalanche-2"
target_price = 185
monitor_crypto_price(coin_id, target_price)
