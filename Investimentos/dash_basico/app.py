from flask import Flask, render_template, request
import yfinance as yf

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/compare', methods=['POST'])
def compare():
    # Obter os dados do formulário
    symbol = request.form.get('symbol')
    quantity = float(request.form.get('quantity'))
    purchase_price = float(request.form.get('purchase_price'))

    # Obter dados do yfinance para a ação específica
    try:
        stock_data = yf.Ticker(symbol)
        current_price = stock_data.history(period='1d')['Close'].iloc[-1]
        current_value = current_price * quantity
        total_investment = purchase_price * quantity
        difference_percentage = ((current_value - total_investment) / total_investment) * 100
    except (ValueError, KeyError):
        difference_percentage = "Erro ao obter dados do yfinance ou processar os dados. Certifique-se de inserir dados válidos."

    return render_template('result.html', difference_percentage=difference_percentage)

if __name__ == '__main__':
    app.run(debug=True)
