from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/compare', methods=['POST'])
def compare():
    # Obter os dados do formulário
    metatrader_data = request.form.get('metatrader_data')
    manually_entered_data = request.form.get('manually_entered_data')

    # Realizar a lógica de comparação aqui (por exemplo, calcular a diferença em porcentagem)
    # Substitua este bloco de código com sua lógica específica
    try:
        metatrader_data = float(metatrader_data)
        manually_entered_data = float(manually_entered_data)
        difference_percentage = ((metatrader_data - manually_entered_data) / manually_entered_data) * 100
    except ValueError:
        difference_percentage = "Erro ao processar os dados. Certifique-se de inserir números válidos."

    return render_template('result.html', difference_percentage=difference_percentage)

if __name__ == '__main__':
    app.run(debug=True)
