from pandas_datareader import data as pdr
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import pandas as pd
from scipy.optimize import minimize
import matplotlib.ticker as mtick

lista_acoes = ["BBAS3", "BBDC3", "BBSE3", "BRIT3", "CMIG3", "CPLE3", "GGBR3", "GOAU3", "ITSA3", "ITUB3", "KLBN3", "SAPR3", "TAEE3"]

#precos = pdr.get_data_yahoo(lista_acoes, start=dt.datetime(2019, 1, 1), end=dt.datetime(2023, 12, 1))['Adj Close']
data = pdr.get_data_yahoo(lista_acoes, start=dt.datetime(2019, 1, 1), end=dt.datetime(2023, 12, 1))
print(data.keys())

precos = data['Adj Close']

retornos = precos.pct_change().apply(lambda x: np.log(1+x)).dropna()
media_retornos = retornos.mean()
matriz_cov = retornos.cov()


numero_carteiras = 100000
vetor_retornos_esperados = np.zeros(numero_carteiras)
vetor_volatilidades_esperadas = np.zeros(numero_carteiras)
vetor_sharpe = np.zeros(numero_carteiras)
tabela_pesos = np.zeros((numero_carteiras, len(lista_acoes)))

for k in range(numero_carteiras):
    pesos = np.random.random(len(lista_acoes))
    pesos /= np.sum(pesos)
    tabela_pesos[k, :] = pesos

    vetor_retornos_esperados[k] = np.sum(media_retornos * pesos * 252)
    vetor_volatilidades_esperadas[k] = np.sqrt(np.dot(pesos.T, np.dot(matriz_cov*252, pesos))) 

    vetor_sharpe[k] = vetor_retornos_esperados[k] / vetor_volatilidades_esperadas[k]


posicao_sharpe_maximo = vetor_sharpe.argmax()
pesos_otimos = tabela_pesos[posicao_sharpe_maximo, :]
pesos_otimos = [str((peso*100).round(2)) + "%" for peso in pesos_otimos]

for i, acao in enumerate(lista_acoes):
    print(f"{acao}: {pesos_otimos[i]}")

vetor_retornos_esperados_arit = np.exp(vetor_retornos_esperados) - 1
eixo_y_fronteira_eficiente = np.linspace(vetor_retornos_esperados_arit.min(), vetor_retornos_esperados_arit.max(), 50)


def pegando_retorno(peso_teste):
    peso_teste = np.array(peso_teste)
    retorno = np.sum(media_retornos * peso_teste) * 252
    retorno = np.exp(retorno) - 1
    return retorno

def checando_soma_pesos(peso_teste):
    return np.sum(peso_teste) - 1

def pegando_vol(peso_teste):
    peso_teste = np.array(peso_teste)
    vol = np.sqrt(np.dot(peso_teste.T, np.dot(matriz_cov*252, peso_teste)))
    return vol

peso_inicial = [1/len(lista_acoes)] * len(lista_acoes)
limites = tuple([(0, 1) for ativo in lista_acoes])
eixo_x_fronteira_eficiente = []

for retorno_possivel in eixo_y_fronteira_eficiente:
    restricoes = ({'type': 'eq', 'fun': checando_soma_pesos},
    {'type': 'eq', 'fun': lambda w: pegando_retorno(w) - retorno_possivel}),
    result = minimize(pegando_vol, peso_inicial, args=(), method='SLSQP', bounds=limites, constraints=restricoes)
    eixo_x_fronteira_eficiente.append(result['fun'])


fig, ax = plt.subplots()
ax.scatter(vetor_volatilidades_esperadas, vetor_retornos_esperados_arit, c=vetor_sharpe, cmap='viridis')
plt.xlabel('Volatilidade Esperada')
plt.ylabel('Retorno Esperado')
ax.scatter(vetor_volatilidades_esperadas[posicao_sharpe_maximo], vetor_retornos_esperados_arit[posicao_sharpe_maximo], c='red', marker='*')
ax.plot(eixo_x_fronteira_eficiente, eixo_y_fronteira_eficiente, 'r--')
ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
ax.xaxis.set_major_formatter(mtick.PercentFormatter(1.0))
plt.show