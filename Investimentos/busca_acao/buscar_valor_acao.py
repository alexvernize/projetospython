import requests
import time

# Defina o valor médio que você pagou pela ação PETR4 em reais
valor_medio_petr4 = 38.60  # Exemplo fictício, substitua pelo seu valor médio

# Defina a porcentagem de lucro desejada (entre 10% e 20%)
porcentagem_lucro = 0.10

# Função para obter o preço atual da ação PETR4 em reais
def obter_preco_petr4():
    api_key = 'F2C9KJWELMMBFPTC'  # Substitua pela sua chave de API da Alpha Vantage
    url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=PETR4.SAO&apikey={api_key}'
    response = requests.get(url)
    data = response.json()
    preco_atual = float(data['Global Quote']['05. price'])
    return preco_atual

# Função para verificar se o preço atingiu a meta de lucro
def verificar_meta_petr4(preco_atual, valor_medio):
    meta_lucro = valor_medio * (1 + porcentagem_lucro)
    if preco_atual >= meta_lucro:
        print(f'Ação PETR4 atingiu a meta de lucro! Preço atual: R$ {preco_atual:.2f}')

# Loop principal
while True:
    preco_atual_petr4 = obter_preco_petr4()
    print(f'\nPreço atual da ação PETR4: R$ {preco_atual_petr4:.2f}')
    verificar_meta_petr4(preco_atual_petr4, valor_medio_petr4)
    # Aguarde 60 segundos antes de verificar novamente
    time.sleep(60)
