#Biblioteca paramiko "pip install paramiko"
import paramiko

address = "192.168.0.88"

username = "master"

password = "alex989109"

#Instanciar paramiko na variável ssh
ssh = paramiko.SSHClient()

#Aceitar o a chave do host para não precisar digitar o yes e ele já adicionar no known_hosts
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

#A conexão 
ssh.connect(hostname=address, username=username, password=password)

#Definir os retornos
stdin, stdout, stderr = ssh.exec_command("ifconfig")
stdin.close()

interfaces = []

#Dessa frma consigo ver o texto do ifconfig linha a linha
for line in stdout.readlines():
    #Com o split é removida as linhas a partir daquele caracter
    #print(line.split("\n"))
    #Com o replace eu substituo caracteres
    result = line.replace("\n", "")

    #Pegando o nome da interface, repare que eu pego via lista no index 0, porque ele retorna uma lista
    #então eu pego o primeiro elemento.
    if "mtu" in line:
        interface_name = result.split(":")[0]
        interfaces.append(interface_name)

ssh.close()

print(interfaces)
