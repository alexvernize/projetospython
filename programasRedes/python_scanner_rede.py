#Biblioteca do nmap para instalar pip install python-nmap
#Também é necessário o nmap instalado na máquina com o apt install nmap
import nmap

#Qual o ip que quero buscar as portas
destino = "192.168.0.0-255"

#Porta que quero scannear
porta = "22"

#Variável para o scanner
nm = nmap.PortScanner()

#Scan propriamente dito, nele passamos o endereço e a porta
#nm.scan(hosts="192.168.0.46", ports="22")
nm.scan(hosts=destino, ports=porta)
if nm.all_hosts():
    print("Host encontrado")
    print(nm.all_hosts())
    for host in nm.all_hosts():
        host_ip = host
        host_state = nm[host_ip].state()
        port_state = nm[host_ip]["tcp"][int(porta)]["state"]
        print(f"{host_ip} - {host_state} - {port_state}")
else:
    print("Host não encontrado")
