import random

mensagens = ['Não procure ser o melhor, mas sim o mais simples. Porque até a maior ' + \
        'árvore da floresta começa do chão.', 'Se você cansar, aprenda a descansar ' + \
        'e não a desistir.', 'Toda mente é um cofre. Não existem mentes impenetráveis, ' + \
        'apenas chaves erradas.', 'Não adianta encher de perfume se o que falta é a ' + \
                'essência.', 'Troque suas folhas, mas não perca suas raízes. Mude suas ' + \
                'opiniões,mas não perca seus princípios.', 'Ignorar é a forma mais ' + \
                'elegante de se defender da maldade.', 'Não faça da sua vida um rascunho, ' + \
                'poderá não ter tempo de passá-la a limpo.', 'A paciência é a chave para ' + \
                'todos os problemas que não dependem de você.', 'Não haverá borboletas se ' + \
                'a vida não passar por longas e silenciosas metamorfoses.', 'Reaja com ' + \
                'inteligência mesmo quando for tratado com ignorância.', 'Saber esperar é ' + \
                'uma virtude. Aceitar, sem questionar, que cada coisa tem um tempo certo ' + \
                'para acontecer, é ter fé!', 'Primeiro a chuva, depois o arco-íris. Se ' + \
                'acostume, a ordem é sempre essa.', 'Tudo é uma questão de ponto de vista: ' + \
                'pra formiga, gota de chuva é tsunami.', 'Inteligente não é quem sabe para ' + \
                'onde ir, mas quem aprendeu para onde não deve voltar.', 'Nunca desista de ' + \
                'um sonho só por causa do tempo que você vai levar para realizá-lo. O tempo ' + \
                'vai passar de qualquer forma.']

print(mensagens[random.randint(0, len(mensagens) - 1)]) 

'''
Observe a expressão usada como índice de messages : random.randint(0,len(mensagens) - 1). 
Isso gera um número aleatório a ser usado como índice, independentemente do tamanho de 
mensagens , ou seja, você obterá um número aleatório entre 0 e o valor len(messages) - 1.
Dessa forma ele vai ler a lista e descobrir quantos valores ela possui, assim se quiser 
adicionar mais valores não é necessário mudar essa parte do código.
'''

