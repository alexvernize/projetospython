'''
Programa para armazenar as datas dos aniversários, utilizando dicionários.
'''

aniversarios = {'Dayane': '04 de Julho', 'Alex': '20 de Setembro', 'Mathe' + \
        'us': '19 de Agosto', 'Stefany': '17 de Janeiro'}

while True:
    print('Digite o nome: (Ou tecle enter para sair)')
    nome = input()
    if nome == '':
        break
    if nome in aniversarios:
        print(aniversarios[nome] + ' é o aniverário dele(a) ' + nome)
    else:
        print('Eu não tenho informações sobre o aniversário dele(a) ' + nome)
        print('Quando é o aniversário dele(a)? ')
        niver = input()
        aniversarios[nome] = niver
        print('Aniversário salvo')
