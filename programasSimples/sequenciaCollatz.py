

try:
    def collatz(numero):
        if numero % 2 == 0:
            return numero // 2
        else:
            return 3 * numero + 1

    print('Digite um número: ')
    valor = int(input())

    while valor > 1:
        valor = collatz (valor)
        print(valor)

except ValueError:
        print('Digite um número inteiro.')

