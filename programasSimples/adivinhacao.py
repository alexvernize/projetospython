#Jogo de adivinhação de número.

import random

numeroSecreto = random.randint(1 ,20)
print('Estou pensando em um número entre 1 e 20, você pode tentar descobrir com seis tentativas.')

#laço pedindo para o jogador adivinhar 6 vezes.

for adivinhar in range(1, 7):
    print('Adivinhe.')
    adivinhe = int(input())

    if adivinhe < numeroSecreto:
        print('Seu número está abaixo do meu.')
    elif adivinhe > numeroSecreto:
        print('Seu número está acima do meu.')
    else:
        break 
if adivinhe == numeroSecreto:
    print('Bom trabalho! Você conseguiu adivinhar o meu número em ' + str(adivinhar) + ' tentativas!')

else:
    print('Deu ruim. O número que eu estava pensando era o ' + str(numeroSecreto))
