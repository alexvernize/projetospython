'''
Software para salvar nomes em listas utilizando o laço
while.
'''

nomeAmigos = []

while True:
    print('Digite o nome do seu amigo' + str(len(nomeAmigos)+1) + '(Ou aperte enter para sair): ')
    nome = input()
    if nome == '':
        break
    nomeAmigos = nomeAmigos + [nome] #concatenação de lista
print('O nome dos seus amigos são: ')
for nome in nomeAmigos:
    print('' + nome)
