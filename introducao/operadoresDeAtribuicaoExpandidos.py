'''
É possível usar o operador de atribuição expandido para não precisar
repetir as variáveis. Há operadores expandidos para os operadores +,
-, *, / e %. Veja abaixo.
'''
#Essa é uma forma de atribuir valor a uma variável
a = 42
print('a = ', a)
a = a + 1

#Dessa forma fazemos o mesmo que o código acima
b = 42
print('b = ', b)
b += 1

print('a = a + 1: ', a)
print('b += 1: ', b)

#Isso valor para strings e listas também

x = 'Hello'
print('x = ', x)
x += 'World'
print('x += World: ', x)
lista = ['Bola']
print('lista = ', lista)
lista *= 3
print('lista *= 3: ', lista)
