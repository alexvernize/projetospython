'''
É possível dividir as instruções em várias linhas, para isso 
usamos o caracter de soma '+' seguido do barra contrária \.
Veja abaixo um exemplo.
'''

print('Hello ' + \
        'World')
