'''
A função print pode receber alguns argumentos, dentre esses argumentos três
parâmetros são passados ao final com as palavras sep, end e file. Esses 
parâmetros não são vistos porque eles já possuem valores pré definidos em 
python. 
'''

#sep = possui um valor padrão de espaço em branco
#end = possui um valor padrão de pular linha o famoso \n
#file = Este é o padrão que define onde será impresso os argumentos passados para
#print. Seu valor padrão é definido por sys.stdout, o fluxo de saída padrão normalmente
#é o terminal.

!/usr/bin/env python3
# Alterando os valores de sep e end na função print

ano1 = '1980'
ano2 = '1990'
ano3 = '2000'
ano4 = '2010'

texto = "Alterando o valor de sep"
print(texto)
print(ano1, ano2, ano3, ano4, sep='--->')

# pula uma linha
print()

texto = "Alterando o valor de sep e end"
print(texto)
print(ano1, ano2, ano3, ano4, sep='--->', end='...\n')
