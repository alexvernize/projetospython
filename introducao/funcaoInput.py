'''
A função input() faz uma pausa no programa e espera uma entrada do usuário pelo terminal. 
Para ler a entrada do usuário a função input() espera que após digitada a entrada o 
usuário aperte a tecla enter, após isso input() lê essa entrada como uma string, portanto, 
se a entrada esperada for um número ela deve ser convertida usando-se as funções de 
conversão int() ou float().
'''

#!/usr/bin/env python3
# Programa para calcular a media de um aluno

print('Programa para calcular a media de um aluno')
print()

nome = input('Entre com o nome do aluno: ')
print()

nota1 = float(input("Entre com a primeira nota: "))
print()

nota2 = float(input("Entre com a segunda nota: "))
print()

media = (nota1 + nota2)/2
print(nome, 'teve media igual a:', media)
