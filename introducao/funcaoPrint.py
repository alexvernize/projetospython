#!/usr/bin/env python3
# Programa para converter graus de Celsius para Fahrenheit

c = 25 f = 1.8*c + 32 

'''
A função print serve para imprimir argumentos passados a ela no terminal.
A grande diferenca entre da função print no python 2 para o 3 é que em 
python 3 ela deve ser obrigatoriamente estar entre parênteses.
'''
print('25 graus Celsius = ', f, ' graus Fahrenheit')
