'''
Existem duas formas de fazer comentários em python, uma é utilizando 
três aspas simples antes de depois do comentário que deseja digitar, 
como aqui nesse exemplo, dessa forma podemos fazer várias linhas de 
comentários dentro dessas aspas simples. 
'''

#Outra forma de fazer comentários é utilizando o símbolo de sustenido
#Como estamos fazendo, a diferença para as aspas simples é que nesse
#tipo de comentário podemos escrever apenas uma linha, então cada vez 
#que vamos comentar precisamos utilizar o símbolo no início da frase.
