#Para utilizar as várias bibliotecas que o python dispõem e com
#elas chamar diversas funções, podemos importar os módulos que 
#queremos utilizar. A importação é bem simples. Utilizamos a 
#palavra-chave 'import' mais o nome do módulo que deseja utilizar.
#É possível chamar diversos módulos numa mesma linha os separando
#por uma vírgula.

import random
for i in range(5):
    print(random.randint(1, 10))
