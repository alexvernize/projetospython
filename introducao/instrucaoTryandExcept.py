def divisao(valor):
    try:
        return 10/valor
    except ZeroDivisionError:
        print('Divisão por zero não permitida, tente outro número.')

print(divisao(2))
print(divisao(10))
print(divisao(0))
print(divisao(5))
