'''
O programa a seguir apresentará um erro porque a variável eggs só existe no escopo 
local criado spam() é chamado.
'''
#def spam():
#    eggs = 31337
#spam()
#print(eggs)

'''
Variáveis locais não podem usar variáveis de outros escopos locais.
Um novo escopo é criado sempre que uma função é chamada, inclusive
quando uma função é chamada a partir de outra função
'''

#def spam():
#    eggs = 99
#    bacon()
#    print(eggs)
#def bacon():
#    ham = 101
#    eggs = 0
#spam()

'''
Variáveis globais podem ser lidas a partir de um escopo local. No exemplo
como não há nenhum parâmetro chamado eggs nem qualquer código que 
atribua um valor a eggs na função spam(), o python considera como referência 
a variável global eggs com o valor de 42.
'''

#def spam():
#    print(eggs)
#eggs=42
#spam()
#print(eggs)

'''
É possível utilizar o mesmo nome de variável para variáveis locais e globais, 
porém para simplicar a sua vida, evite utilizar nomes iguais.
'''

#def spam():
#    eggs = 'spam local'
#    print(eggs) #exibe 'spam local'

#def bacon():
#    eggs = 'bacon local'
#    spam()
#    print(eggs) #exibe 'bacon local'
#    print(eggs) #exibe 'bacon local'

#eggs = 'global'
#bacon()
#print(eggs) #exibe 'global'

'''
É possível modificar uma variável global em uma função, para isso utilize a instrução
'global'. Para isso nunca crie uma variável local com esse nome. Abaixo um exemplo.

'''

#def spam():
#    global eggs
#    eggs = 'spam'

#eggs = 'global'
#spam()
#print(eggs)



