'''Funcoes possuem parametros opcionais que podem ou nao
   utilizados junto a funcao. Por exemplo a funcao print()
   possue os parametros opcionais end e sep'''

print('Hello')
print('World')

#No exemplo acima nao utilizamos nenhum parametro e por padrao
#a funcao print adiciona um caractere de quebrab de linha, abaixo
#utilizamos o parametro opcional end e nao teremos a quebra de linha.
print('Hello', end='')
print('World')

#De modo semelhante ao passar diversos valores de string na funcao print
#ela as separara automaticamente com um espaco, porem eh possivel substituir
#a string default passando um argumento no parametro opcional sep, como abaixo

print('cats', 'dogs', 'mice')
print('cats', ' dogs', ' mice', sep = ',')
