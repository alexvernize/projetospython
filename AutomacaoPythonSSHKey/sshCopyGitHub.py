from pathlib import Path
import os
from socket import timeout
from time import sleep
from typing import KeysView
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pyperclip
import getpass

if Path('/Users/alex.prado/.ssh/id_rsa.pub').is_file():
    print ("File exist")
    navegador = webdriver.Firefox()
    navegador.get('https://github.com/login')

    #signin = navegador.find_element_by_link_text('Sign in')  
    #signin.click()
    #Criptografar dados e esconder dados
    windowMaximize = navegador.maximize_window()
    usuario = input("Qual seu usuário? ")
    login = navegador.find_element_by_id("login_field").send_keys(usuario)
    #senha = input("Qual sua senha? ")
    senha = getpass.getpass("Digite sua senha: \n")
    password = navegador.find_element_by_id("password").send_keys(senha)
    botaoNext = navegador.find_element_by_xpath("//*[starts-with(@class, 'btn')]")          
    botaoNext.click()
    sleep(1)
    avatar = navegador.find_element_by_xpath("/html/body/div[1]/header/div[7]/details/summary/img").click()
    sleep(1)
    config = navegador.find_element_by_xpath("/html/body/div[1]/header/div[7]/details/details-menu/a[8]").click()
    configSSH = navegador.find_element_by_xpath("/html/body/div[5]/main/div/div[2]/div[1]/div/action-list/nav/ul/li[8]/ul/li[4]/a/span[2]").click()
    fo = open('/Users/alex.prado/.ssh/id_rsa.pub', 'r').read()
    pyperclip.copy(fo)
    addKey = navegador.find_element_by_xpath("/html/body/div[5]/main/div/div[2]/div[2]/div/div/div[1]/div/a").click()

    #Pedir o nome da chave
    titulo = input("Qual nome deseja colocar? \n")
    navegador.find_element_by_id("public_key_title").send_keys(titulo)
    navegador.find_element_by_id("public_key_key").send_keys(Keys.COMMAND + 'v')
    sleep(10)
    navegador.find_element_by_xpath("/html/body/div[5]/main/div/div[2]/div[2]/div/div/form/p/button").click() 
    sleep(2)
    #navegador.close()
else:
    print ("File not exist")

try:
    with open('/Users/alex.prado/.ssh/id_rsa.pub') as f:
        print(f.readlines())
        # Do something with the file
except IOError:
    print("File not accessible")
