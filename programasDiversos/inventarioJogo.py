'''
Código para um jogo fictício de fantasia do livro automatize as 
tarefas com o python.
'''
import pprint

inventario = {'cordas': 2, 'tochas': 7, 'arcos': 4, 'flechas': 20, 'moedas de ouro': 50}

def displayInventario(lerInventario):
    print("Inventário:")
    item_total = 0
    for i, j in lerInventario.items():
        #contador.setdefault(i, 0)
        #contador[i] = contador[i] + 1
        print(i + ':' + ' ' + str(j))
        item_total += j
    print("\nNúmero total de itens no inventário: " + str(item_total))

displayInventario(inventario)        
