'''
Código de jogo da velha escrito em python, usando conceitos de 
dicionario e strings.
'''

oTabuleiro = {'top-E': '', 'top-M': '', 'top-D': '',
             'meio-E': '', 'meio-M': '', 'meio-D': '',
             'baixo-E': '', 'baixo-M': '', 'baixo-D': ''}


def printTabuleiro(tabuleiro):
    print(tabuleiro['top-E'] + '|' + tabuleiro['top-M'] + '|' + tabuleiro['top-D'])
    print('-+-+-')
    print(tabuleiro['meio-E'] + '|' + tabuleiro['meio-M'] + '|' + tabuleiro['meio-D'])
    print('-+-+-')
    print(tabuleiro['baixo-E'] + '|' + tabuleiro['baixo-M'] + '|' + tabuleiro['baixo-D'])

turno = 'X'

for i in range(9):
    printTabuleiro(oTabuleiro)
    print('Turno para ' + turno + '. Mover para qual espaço?')
    move = input()
    oTabuleiro[move] = turno
    if turno == 'X':
        turno = 'O'
    else:
        turno = 'X'
printTabuleiro(oTabuleiro)
