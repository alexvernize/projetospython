# -*- coding: utf-8 -*-
#! python3
# lucky.py – Abre vários resultados de pesquisa no Google.
#Pagina 345 automatize tarefas macantes

"""
Eis o que o seu programa deve fazer:
• Obter palavras-chave para pesquisa a partir dos argumentos da linha de comando.
• Obter a página com os resultados da pesquisa.
• Abrir uma aba do navegador para cada resultado.

Isso significa que seu código deverá fazer o seguinte:
• Ler os argumentos da linha de comando em sys.argv.
• Acessar a página com os resultados da pesquisa usando o módulo requests.
• Encontrar os links para cada resultado da pesquisa.
• Chamar a função webbrowser.open() para abrir o navegador web.
"""
import requests, sys, webbrowser, bs4

# exibe um texto enquanto faz download da página do Google
print('Googlando...')

res = requests.get('https://www.google.com/search?q=' + ''.join(sys.argv[1:]))
res.raise_for_status()

# Obtém os links dos principais resultados da pesquisa.
soup = bs4.BeautifulSoup(res.text, features="html5lib")

# Abre uma aba do navegador para cada resultado.
linkElems = soup.select('.r a')#Elemento <a> com a classe <.ellip> que eh o padrao de links de pesquisa
"""A função interna min() do Python retorna o menor argumento inteiro ou de ponto flutuante que receber. Há também uma
função max() interna que retorna o maior argumento recebido."""
numOpen = min(5, len(linkElems))
for i in range(numOpen):
    webbrowser.open('https://google.com' + linkElems[i].get('href'))
