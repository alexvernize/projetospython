#! python3
# backupToZip.py – Copia uma pasta toda e seu conteúdo para um arquivo ZIP cujo nome seja incrementado.
#pg 293 livro automatize tarefas maçantes

"""
O código desse programa será colocado em uma função chamada backupToZip() . Isso facilitará copiar e colar a função
em outros programas Python que precisem dessa funcionalidade. No final do programa, a função será chamada para
realizar o backup.
"""
import zipfile, os

def backupParaZip(pasta):

    #Faz backup de toda pasta em um arquivo ZIP.

    # garante que a pasta é um path absoluto
    pasta = os.path.abspath(pasta)

    # Determina o nome do arquivo que esse código deverá usar de acordo com os arquivos já existentes.
    numeroDeBackup = 1
    while True:
        nomeArquivoZip = os.path.basename(pasta) + '_' + str(numeroDeBackup) + '.zip'
        if not os.path.exists(nomeArquivoZip):
            break
        numeroDeBackup = numeroDeBackup + 1

    #Cria o arquivo Zip.
    print('Criando %s...'%(nomeArquivoZip))
    backupZip = zipfile.ZipFile(nomeArquivoZip, 'w')

    # Percorre toda a árvore de diretório e compacta os arquivos de cada pasta.
    for foldername, subfolders, filenames in os.walk(pasta):
        print('Adicionando arquivos em %s...' % (foldername))

    # Acrescenta a pasta atual ao arquivo ZIP.
    backupZip.write(foldername)

    # Acrescenta todos os arquivos dessa pasta ao arquivo ZIP.
    for filename in filenames:
        newBase = os.path.basename(pasta) + '_'
        if filename.startswith(newBase) and filename.endswith('.zip'):
            continue  # não faz backup dos arquivos ZIP de backup

        backupZip.write(os.path.join(foldername, filename))
    backupZip.close()


    print('Feito')

backupParaZip('/home/alex/teste')