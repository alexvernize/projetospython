#Não esquecer de instalar pacote
#pip3 install psutil

import psutil 

#Captura sensor da bateria
battery = psutil.sensors_battery()

#Captura o percentual da bateria
percent = str(battery.percent)

#Mostra o resultado
print(f'No momento você tem {percent}% de bateria!')