import os
import yaml
from collections import namedtuple

from putify.auth import AuthMethod

Config = namedtuple("Config", ["client_id", 
                               "client_secret", 
                               "access_token_url", 
                               "auth_url", 
                               "api_version", 
                               "api_url", 
                               "base_url"
])

def read_config():
    current_dir = os.path.abspath(os.curdir)
    file_path = os.path.join(current_dir, "config.yaml")

    try:
        whith open(file_path, mode= "r", encoding="UTF8") as file:
        