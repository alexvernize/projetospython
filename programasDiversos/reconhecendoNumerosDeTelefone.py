#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  3 00:49:01 2019

@author: alex
"""

'''
Programa para reconhecer um padrão de número de telefone em um texto.
Esse não é o melhor modo e funciona apenas como demonstração.
'''

def numeroTelefone(texto):
    if len(texto) != 13:
        return False
    for i in range(0, 2):
        if not texto[i].isdecimal():
            return False
    if texto[2] != '-':
        return False
    for i in range (3 ,8):
        if not texto[i].isdecimal():
            return False
    if texto[8] != '-':
        return False
    for i in range (9, 13):
        if not texto[i].isdecimal():
            return False
    return True

#print ('41-99820-3053 é um número de celular: ')
#print(numeroTelefone('41-99820-3053'))
#print('12345674124554 é um número de telefone: ')
#print(numeroTelefone('12345674124554'))

mensagem = "Oi Álex, meu número de celular é 41-99599-3015"

for i in range(len(mensagem)):
    numero = mensagem[i:i+13]
    if numeroTelefone(numero):
        print("Número de telefone encontrado: " + numero + '\n')
print("Finalizado")