#! python3
# renameDates.py – Renomeia os nomes de arquivo com formato de data MM-DD-AAAA em estilo # americano para o
# formato DD-MM-AAAA em estilo brasileiro.
#pg 288 automatize tarefas maçantes

'''
Eis o que o programa deve fazer:
• Procurar todos os nomes de arquivo no diretório de trabalho atual em busca de datas em estilo americano.
• Quando um arquivo for encontrado, ele deverá ser renomeado com o mês e o dia trocados para deixar a data
em estilo europeu.

Isso significa que o código deverá fazer o seguinte:
• Criar uma regex que possa identificar o padrão de texto para datas em estilo americano.
• Chamar os.listdir() para encontrar todos os arquivos no diretório de trabalho.
• Percorrer todos os nomes de arquivo em um loop usando a regex para verificar se ele contém uma data.
• Se houver uma data, o arquivo deverá ser renomeado com shutil.move().
'''

import shutil, os, re

#Cria uma regex que corresponda aos arquivos com formato de data em estilo americano.
dataUSA = re.compile(r"""^(.*?) #Tudo o que tiver de texto antes da data
((0|1)?\d)-                     #um ou dois dígitos para o mês
((0|1|2|3)?\d)-                 #um ou dois dígitos para o dia
((19|20)\d\d)                   #quatro dígitos para o ano
(.*?)$                          #Tudo o que tiver de texto após a data
 """, re.VERBOSE)#re.VERBOSE permite espaços em branco e comentários na regex

# Percorre os arquivos do diretório de trabalho com um loop.
for usNomeArquivo in os.listdir('.'):
    mo = dataUSA.search(usNomeArquivo)

    #Ignora os arquivos que não tenham uma data.
    if mo == None:
        continue

    #Obtém as diferentes partes do nome do arquivo.
    beforePart = mo.group(1)
    monthPart = mo.group(2)
    dayPart = mo.group(4)
    yearPart = mo.group(6)
    afterPart = mo.group(8)

    # Compõe o nome do arquivo em estilo brasileiro.
    dataBr = beforePart + dayPart + '-' + monthPart + '-' + yearPart + afterPart

    # Obtém os paths absolutos completos dos arquivos.
    absWorkingDir = os.path.abspath('.')
    usNomeArquivo = os.path.join(absWorkingDir, usNomeArquivo)
    dataBr = os.path.join(absWorkingDir, dataBr)

    # Renomeia os arquivos.
    print('Renaming "%s" to "%s"...' % (usNomeArquivo, dataBr))#printa os arquivos que serao renomeados
    #shutil.move(usNomeArquivo, dataBr) # remova o caractere de comentário quando tiver ctz que pode renomear