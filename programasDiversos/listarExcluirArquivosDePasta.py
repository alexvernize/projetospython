#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

@author: alexvernize

Software para listar ou apagar os arquivos com uma extensão específica de uma pasta. Forma de utilizar.

1 - Coloque o path (caminho) até a pasta que deseja ser listada.
2 - Se quiser apenas listar, coloque o '#' no comando os.unlink(i) e tire o '#' do comando print.
3 - Para apagar os arquivos .txt faça o inverso do item anterior.

"""

import os 
for i in os.listdir('/home/alex/teste/'):
    #Como exemplo usamos o arquivo .txt, mas é possivel usar qualquer extensão.
    if i.endswith('.txt'):
        #os.unlink(i)
        print(i)
