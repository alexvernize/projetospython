#! python3
#Programa que acrescenta marcadores no início de cada linha de texto
#copiada no clipboard

'''
Esse script obterá o texto do clipboard, adicionará um asterisco e um
espaço no início de cada linha e, em seguida, colará esse novo texto
no clipboard. Por exemplo se eu copiar o texto a seguir:

Listas de animais
Listas de frutas
Listas de carros
Listas de celulares

Depois de executar o script teremos no clipboard:

* Listas de animais
* Listas de frutas
* Listas de carros
* Listas de celulares
'''

#Passos do programa

#1 - Obter o texto do clipboard
#2 - Fazer algo com esse texto
#3 - Copiar o novo texto para o clipboard

import pyperclip

#Passo 1
#Copiando texto do clipboard
texto = pyperclip.paste()

#Passo 2
#Separa as linhas e acrescenta os asteriscos
#Devolve o texto em uma lista, com a separação nas quebras de linha
linhas = texto.split('\n')
#Percorre todos os índices da lista 'linhas' em um loop
for i in range(len(linhas)):
    #Acrescenta um asterisco em cada string da lista 'linhas'
    linhas[i] = '* ' + linhas[i]
#O comando pyperclip.copy está esperando um único valor de string, como
#criamos uma lista com o método split, precisamos, voltar a um único valor
#de string, usando o método join() conseguimos isso.
texto = '\n'.join(linhas)

#Passo 3
#Colando texto do clipboard
pyperclip.copy(texto)


