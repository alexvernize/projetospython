#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 21:37:05 2019

@author: alex
"""

#Um programa para repositório de senhas que não é seguro

#Módulo de argumentos de linha de comando
import sys, pyperclip

senhas = {'hotmail': 'Hlasj26hGiL',
          'facebook': 'LkjsY675Tbasj',
          'instagram': '1shhHlPoJUJ345'}

#Os argumentos da linha de comando serão armazenados na variável sys.argv. Um exemplo
#comum de uso de argumento de linha de comando é quando utilizamos algum comando do
#Linux e adicionamos um --help com o comando, por exemplo: $ls --help.

#Esse comando if serve para se for digitado apenas o nome do programa ele devolver uma
#mensagem ensinando como usar o software
if len(sys.argv) < 2:
    #O primeiro item sempre será uma string contendo o nome do programa [0]
    #O segundo item deverá ser o primeiro argumento da linha de comando. Nesse
    #programa esse argumento será o nome da conta cuja senha você deseja obter.
    print('Uso: python3 programaGerenciadorDeSenhas.py [conta]- cópia senha da conta')
    sys.exit()

conta = sys.argv[1] #O primeiro argumento da linha de comando é o nome da conta

if conta in senhas:
    #Esse comando copia a senha para o clipboard
    pyperclip.copy(senhas[conta])
    print('A senha para ' + conta + ' foi copiada para o clipboard.')
else:
    print('Não existe nenhuma conta com esse nome ' + conta)


