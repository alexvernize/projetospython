#! python3
#Salva e carrega porções de texto no clipboard.

# Uso: py.exe mcb.pyw save <palavra-chave> – Salva clipboard na palavra-chave.
#py.exe mcb.pyw <palavra-chave> - Carrega palavra-chave no clipboard.
#py.exe mcb.pyw list – Carrega todas as palavras-chave no clipboard.

"""
Programa que pode ser usado para controlar os textos do clipboard. A extensão .pyw quer dizer que o Python
não mostrará uma janela do Terminal quando executar esse programa. O programa salvará cada porção de texto
do clipboard com uma palavra-chave. Por exemplo, ao executar py mcb.pyw save spam , o conteúdo atual do
clipboard será salvo com a palavra-chave spam. Esse texto poderá ser posteriormente carregado para o clipboard
novamente se py mcb.pyw spam for executado. Se o usuário se esquecer das palavras-chave existentes, ele poderá
executar py mcb.pyw list para que uma lista de todas as palavras-chaves seja copiada para o clipboard.

Eis o que o programa faz:
• O argumento de linha de comando para a palavra-chave é verificado.
• Se o argumento for save , o conteúdo do clipboard será salvo com a palavra-chave.
• Se o argumento for list , todas as palavras-chaves serão copiadas para o clipboard.
• Caso contrário, o texto da palavra-chave será copiado para o clipboard.

Isso significa que o código deverá fazer o seguinte:
• Ler os argumentos de linha de comando em sys.argv .
• Ler e escrever no clipboard.
• Salvar e carregar um arquivo shelf.

Se estiver usando Windows, você poderá executar facilmente esse script a
partir da janela Run... (Executar) criando um arquivo batch chamado mcb.bat
com o conteúdo a seguir:
@pyw.exe C:\Python34\mcb.pyw %*
"""
import shelve, pyperclip, sys

#Sempre que for necessário salvar texto do clipboard, será salva em um arquivo shelf.
mcbShelf = shelve.open('mcb')

#Salva o conteúdo do clipboard
if len(sys.argv) == 3 and sys.argv[1].lower() == 'save':
    mcbShelf[sys.argv[2]] = pyperclip.paste()
elif len(sys.argv) == 2:
    #Lista palavras-chave e carrega conteúdo.
    if sys.argv[1].lower() == list:
        pyperclip.copy(str(list(mcbShelf.keys())))
    elif sys.argv[1] in mcbShelf:
        pyperclip.copy(mcbShelf[sys.argv[1]])
mcbShelf.close()
