#!python3
"""
Programa para localizar números de telefone e emails a partir de textos do clipboard. Esse software irá
substituir o que for colado no clipboard apenas pelo que você está buscando.

Passos a seguir para montar o software:

O software deverá fazer o seguinte:
• Obter o texto do clipboard.
• Encontrar todos os números de telefone e os endereços de email no texto.
• Colá-los no clipboard.

O código deverá fazer o seguinte:
• Usar o módulo pyperclip para copiar e colar strings.
• Criar duas regexes: uma para corresponder a números de telefone e outra para endereços de email.
• Encontrar todas as correspondências, e não apenas a primeira, para ambas as regexes.
• Formatar as strings correspondentes de forma elegante em uma única string a ser colada no clipboard.
• Exibir algum tipo de mensagem caso nenhuma correspondência tenha sido encontrada no texto.
"""
import pyperclip, re

#Regex responsável pelos números
numerosCelularesRegex = re.compile(r'''(
#Essa primeira linha da regex coloca como opcional o telefone ter um código de área com ou sem parênteses.
(\d{3}|\(\d{3}\))?                      # código de área
#Abaixo dizemos ao código que o separador do número pode ser um espaço, '-' ou '.'
(\s|-|\.)?                              # separador
#Abaixo diz que pegamos 5 dígitos após o separador
(\d{5})                                 # primeiros 3 dígitos
#Outra separação de espaço, '-' ou '.'
(\s|-|\.)                               # separador
#Mais quatro dígitos
(\d{4})                                 # últimos 4 dígitos
#Essa última linha é uma extensão opcional composta de qualquer quantidade de espaços seguida de ext , x 
#ou ext. seguida de dois a cinco dígitos.
(\s*(ext|x|ext.)\s*(\d{2,5}))?          # extensão
)''', re.VERBOSE)

#Regex responsável pelos emails
emailRegex = re.compile(r'''(
[a-zA-Z0-9._%+-]+     # nome do usuário
@                     # símbolo @
[a-zA-Z0-9.-]+        # nome do domínio
(\.[a-zA-Z]{2,4})     # ponto seguido de outros caracteres
)''', re.VERBOSE)

#Encontra as correspondências no texto do clipboard.
texto = str(pyperclip.paste())
#Variável que irá armazenar as correspondências
matches = []
for groups in numerosCelularesRegex.findall(texto):
    numeroCelulares = '-'.join([groups[1], groups[3], groups[5]])
    if groups[8] != '':
        numeroCelulares += ' x' + groups[8]
    matches.append(numeroCelulares)
for groups in emailRegex.findall(texto):
    matches.append(groups[0])

# Copia os resultados para o clipboard.
if len(matches) > 0:
    pyperclip.copy('\n'.join(matches))
    print("Copiado para o clipboard:")
    print("\n".join(matches))
else:
    print("Não foi encontrado nenhum número de telefone nem endereço de email.")

"""
Uma ideia de programa semelhantes pode identificar padrões de texto (e possivelmente substituí-los com o método
sub() ) tem várias aplicações diferentes em potencial:

• Encontrar URLs de sites que comecem com http:// ou com https://.
• Limpar datas em formatos diferentes (como 3/14/2015, 03-14-2015 e 2015/3/14) substituindo-as por datas em um 
único formato-padrão.
• Remover informações críticas como números de seguridade social (Social Security Number) ou números de cartões 
de crédito.
• Encontrar erros comuns de digitação como vários espaços entre palavras, repetição acidental acidental de 
palavras ou vários pontos de exclamação no final das sentenças!!!!
"""









