#! python3
#Criador aleatório de questões de provas - Cria provas com perguntas e respostas aleatórias, juntamente com os
#gabaritos contendo as respostas.

"""
Suponha que você seja um professor de geografia, tenha 35 alunos em sua classe e queira aplicar uma prova surpresa
sobre as capitais dos estados brasileiros. Infelizmente, sua classe tem alguns alunos desonestos, e não é
possível confiar neles acreditando que não vão colar. Você gostaria de deixar a ordem das perguntas aleatória para
que cada prova seja única, fazendo com que seja impossível para alguém copiar as respostas de outra pessoa.

Isso é o que o programa faz:

• Cria 35 provas diferentes.
• Cria 50 perguntas de múltipla escolha para cada prova em ordem aleatória.
• Fornece a resposta correta e três respostas incorretas aleatórias para cada pergunta em ordem aleatória.
• Grava as provas em 35 arquivos-texto.
• Grava os gabaritos contendo as respostas em 35 arquivos-texto.

Isso significa que o código deverá fazer o seguinte:

• Armazenar os estados e suas capitais em um dicionário.
• Chamar open() , write() e close() para os arquivos-texto contendo as provas e os gabaritos com as respostas.
• Usar random.shuffle() para deixar a ordem das perguntas e as opções de múltipla escolha aleatórias.

"""
#Importar random para as perguntas e respostas aleatórias
import random

#Dicionário contendo os estados brasileiros como chaves e suas capitais como valores
capitals = {'Acre': 'Rio Branco', 'Alagoas': 'Maceió', 'Amapá': 'Macapá',
'Amazonas': 'Manaus', 'Bahia': 'Salvador', 'Ceará': 'Fortaleza',
'Distrito Federal': 'Brasília', 'Espírito Santo': 'Vitória', 'Goiás': 'Goiânia', 'Maranhão':
'São Luís', 'Mato Grosso': 'Cuiabá', 'Mato Grosso do Sul': 'Campo Grande', 'Minas Gerais': 'Belo Horizonte',
'Pará': 'Belém', 'Paraíba': 'João Pessoa', 'Paraná': 'Curitiba', 'Pernambuco':
'Recife', 'Piauí': 'Teresina', 'Rio de Janeiro': 'Rio de Janeiro', 'Rio Grande do Norte': 'Natal',
'Rio Grande do Sul': 'Porto Alegre', 'Rondônia': 'Porto Velho', 'Roraima': 'Boa Vista',
'Santa Catarina': 'Florianópolis', 'São Paulo': 'São Paulo', 'Sergipe': 'Aracaju', 'Tocantins':
'Palmas'}

#Gerar 35 arquivos contendo provas
for quizNum in range(35):

    #Cria os arquivos com as provas e os gabaritos das respostas
    quizFile = open('/home/alex/Documents/provasCapitais/quizcapitais%s.txt'%(quizNum + 1), 'w')
    answerKeyFile = open('/home/alex/Documents/provasCapitais/gabaritoProva%s.txt'%(quizNum + 1), 'w')

    #Escreve o cabeçalho da prova
    quizFile.write('Nome:\n\nDia:\n\nPeríodo:\n\n')
    quizFile.write((' '*20) + 'Quiz Capitais Estados Brasileiros(Formato %s)'%(quizNum + 1))
    quizFile.write('\n\n')

    #Embaralha a ordem dos estados
    states = list(capitals.keys())
    random.shuffle(states)

#Percorre todos os 27 estados em um loop, criando uma pergunta para cada um.
for questionNum in range(27):

    #Obtém respostas corretas e incorretas.
    correctAnswer = capitals[states[questionNum]]
    wrongAnswer = list(capitals.values())
    del wrongAnswer[correctAnswer.index(correctAnswer)]
    wrongAnswer = random.sample(wrongAnswer, 3)
    answerOptions = wrongAnswer + [correctAnswer]
    random.shuffle(answerOptions)

    # Grava a pergunta e as opções de resposta no arquivo de prova.
    quizFile.write('%s. Qual a capital do Estado de(o) %s?\n' %(questionNum + 1, states[questionNum]))
    for i in range(4):
        quizFile.write(' %s. %s\n'%('ABCD'[i], answerOptions[i]))
    quizFile.write('\n')

    # Grava o gabarito com as respostas em um arquivo.
    answerKeyFile.write('%s. %s\n'%(questionNum + 1, 'ABCD'[answerOptions.index(correctAnswer)]))

quizFile.close()
answerKeyFile.close()

"""
ESSE CÓDIGO ESTÁ COM ALGUM ERRO, DESCOBRIR O QUE. ELE CRIAR OS ARQUIVOS, MAS ESCREVE APENAS NO PRIMEIRO E 
SÓ O TÍTULO, NÃO CRIA AS QUESTÕES.
"""
