#! python3
# mapIt.py – Inicia um mapa no navegador usando um endereço da
# linha de comando ou do clipboard.

"""
Página 325 Automatize tarefas maçantes

Eis o que o seu programa deve fazer:
• Obter um endereço a partir dos argumentos da linha de comando ou do clipboard.
• Abrir o navegador web com a página do Google Maps para o endereço.

Isso significa que seu código deverá fazer o seguinte:
• Ler os argumentos de linha de comando em sys.argv .
• Ler o conteúdo do clipboard.
• Chamar a função webbrowser.open() para abrir o navegador web.
"""
import webbrowser, sys, pyperclip
if len(sys.argv) > 1:
    # Obtém o endereço da linha de comando.
    address = ' '.join(sys.argv[1:])
else:
    #Obtém o endereço do clipboard
    address = pyperclip.paste()

webbrowser.open('https://www.google.com.br/maps/' + address)